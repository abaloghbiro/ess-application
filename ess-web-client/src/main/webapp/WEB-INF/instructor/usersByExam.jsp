<%@page import="hu.braininghub.bh06.ess.common.auth.AccessLevels"%>
<%@page import="hu.braininghub.bh06.ess.dto.BhUserDTO"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="../../shared/error.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>BH Portál - Oktató</title>
<meta name="description" content="">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="robots" content="all,follow">
<!-- Bootstrap CSS-->
<link rel="stylesheet"
	href="resources/vendor/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="resources/vendor/font-awesome/css/font-awesome.min.css">
<!-- Fontastic Custom icon font-->
<link rel="stylesheet" href="resources/css/fontastic.css">
<!-- Google fonts - Poppins -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
<!-- theme stylesheet-->
<link rel="stylesheet" href="resources/css/style.default.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="resources/css/custom.css">
<!-- Favicon-->
<link rel="shortcut icon" href="img/favicon.ico">
<!-- Tweaks for older IEs-->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
<!-- Data Tables -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
	type="text/javascript"></script>
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

</head>
<body>

	<%
		BhUserDTO currentUser = (BhUserDTO) request.getSession().getAttribute("user");

		if (currentUser.getRight().getAccessLevel() != AccessLevels.INSTRUCTOR) {
			throw new RuntimeException();
		}
	%>

	<div class="page">
		<!-- Main Navbar-->
		<header class="header">
			<nav class="navbar">
				<div class="container-fluid">
					<div
						class="navbar-holder d-flex align-items-center justify-content-between">
						<!-- Navbar Header-->
						<div class="navbar-header">
							<!-- Navbar Brand -->
							<a href="instructor" class="navbar-brand">
								<div class="brand-text brand-big">
									<span>BH </span><strong>Hallgatói portál</strong>
								</div>
								<div class="brand-text brand-small">
									<strong>BH</strong>
								</div>
							</a>
							<!-- Toggle Button-->
							<a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
						</div>
						<!-- Navbar Menu -->
						<ul
							class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
							<!-- Logout    -->
							<li class="nav-item"><a href="logout"
								class="nav-link logout">Kijelentkezés<i
									class="fa fa-sign-out"></i></a></li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<div class="page-content d-flex align-items-stretch">

			<!-- Side Navbar -->
			<nav class="side-navbar">

				<!-- Sidebar Header-->
				<div class="sidebar-header d-flex align-items-center">
					<div class="avatar">
						<img src="resources/img/avatar-1.jpg" alt="..."
							class="img-fluid rounded-circle">
					</div>
					<div class="title">
						<h1 class="h4">${sessionScope.user.firstName}</h1>
						<p>Tanár</p>
					</div>
				</div>
				<!-- Sidebar Navidation Menus-->
				<span class="heading">Menü</span>
				<ul class="list-unstyled">


					<li><a
						href="studentsByExam?examId=${examId}&examName=${examName}"> <i
							class="fa fa-file-text"></i>Vizsga adatai
					</a></li>
					<li><a href="instructorPassword"> <i class="fa fa-key"></i>Jelszó
					</a></li>
					<li><a href="instructor"> <i class="fa fa-key"></i>Főoldal
					</a></li>

				</ul>
			</nav>

			<div class="content-inner">

				<!-- Breadcrumb-->
				<section class="tables">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-header d-flex align-items-center">
										<h3 class="h4">
											<c:out value="${examName}"></c:out>
											Vizsga adatai
										</h3>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table class="table table-striped table-hover" id="userTable">
												<thead>
													<tr>
														<th>Vezetéknév</th>
														<th>Keresztnév</th>
														<th>Felhasználónév</th>
														<th>Csoport</th>
														<th>Elmélet pontszám</th>
														<th>Gyakorlat pontszám</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="i" begin="1" end="${examDetails.size()}">
														<c:set var="list" value="${examDetails.get(i)}" />
														<tr>
														<tr>
														<tr>
															<td>${list[0]}</td>
															<td>${list[1]}</td>
															<td>${list[2]}</td>
															<td>${list[3]}</td>
															<td>${list[4]}</td>
															<td>${list[5]}</td>
														</tr>
														<br />
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<!-- Page Footer-->
				<footer class="main-footer">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-6">
								<p>BH05, BH06 &copy; 2017-2018</p>
							</div>
							<div class="col-sm-6 text-right">
								<p>
									Design by <a href="https://bootstrapious.com/admin-templates"
										class="external">Bootstrapious</a>
								</p>
								<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
							</div>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>
	<!-- JavaScript files-->
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/popper.js/umd/popper.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="resources/vendor/jquery.cookie/jquery.cookie.js"></script>
	<script src="resources/vendor/chart.js/Chart.min.js"></script>
	<script src="resources/vendor/jquery-validation/jquery.validate.min.js"></script>

	<script
		src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<!-- Main File-->
	<script src="resources/js/front.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#userTable').DataTable();
			//$( "input" ).addClass( "form-control" );
		});
	</script>
</body>
</html>