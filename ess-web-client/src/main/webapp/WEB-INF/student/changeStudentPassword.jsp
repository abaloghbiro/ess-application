<%@page import="hu.braininghub.bh06.ess.common.auth.AccessLevels"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="hu.braininghub.bh06.ess.dto.ExamAttendanceDTO"%>
<%@page import="hu.braininghub.bh06.ess.dto.BhGroupDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="hu.braininghub.bh06.ess.dto.BhUserDTO"%>
<%@ page errorPage = "../../shared/error.jsp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BH Portál - Adminisztrátor</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="resources/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="resources/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="resources/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="resources/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="resources/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <!-- Data Tables -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

</head>
<body>
<div class="page">
      <!-- Main Navbar-->
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <!-- Navbar Header-->
              <div class="navbar-header">
                <!-- Navbar Brand --><a href="student" class="navbar-brand">
                  <div class="brand-text brand-big"><span>BH </span><strong>Hallgatói portál</strong></div>
                  <div class="brand-text brand-small"><strong>BH</strong></div></a>
                <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
              </div>
              <!-- Navbar Menu -->
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <!-- Logout    -->
                <li class="nav-item"><a href="logout" class="nav-link logout">Kijelentkezés<i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div class="page-content d-flex align-items-stretch"> 
      
      <!-- Side Navbar -->
        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="resources/img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">Bence</h1>
              
             
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Menü</span>
          <ul class="list-unstyled">

            <li><a href="student"> <i class="fa fa-file-text"></i>Vizsgáim</a></li>
            <li><a href="studentPassword"> <i class="fa fa-key"></i>Jelszó</a></li>

          </ul>
        </nav>

        <div class="content-inner">

			<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4">
			  <div class="card">
			    <div class="card-header d-flex align-items-center">
			      <h3 class="h4">Jelszó módosítása</h3>
			    </div>
			    <div class="card-body">
			      <p>Kérem, adja meg új jelszavát.</p>
			      <form method="post" action="studentPassword">
			        <div class="form-group">
			          <label class="form-control-label">Régi jelszó</label>
			          <input type="password" class="form-control" name="oldpass">
			        </div>
			        <div class="form-group">       
			          <label class="form-control-label">Új jelszó</label>
			          <input type="password" class="form-control" name="newpass">
			        </div>
			        <div class="form-group">       
			          <label class="form-control-label">Új jelszó még egyszer</label>
			          <input type="password" class="form-control" name="newpass2">
			        </div>
			        <div class="form-group">       
			          <input type="submit" value="Küldés" class="btn btn-primary">
			        </div>
                		<c:if test="${not empty fn:trim(message)}">
     		  				<c:choose>
     		  					<c:when test="${success}"><div class="alert alert-success" role="alert"><c:out value= "${message}"/></div></c:when>
     		  				<c:otherwise><div class="alert alert-danger" role="alert"><c:out value= "${message}"/></div></c:otherwise>
     		  			</c:choose>
				</c:if>
			      </form>
			    </div>
			  </div>
			</div>
			<div class="col-lg-4"></div>
			</div>

          
          <!-- Page Footer-->
          <footer class="main-footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-6">
                  <p>BH05, BH06 &copy; 2017-2018</p>
                </div>
                <div class="col-sm-6 text-right">
                  <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
                  <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>




<!-- JavaScript files-->
    <script src="resources/vendor/jquery/jquery.min.js"></script>
    <script src="resources/vendor/popper.js/umd/popper.min.js"></script>
    <script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="resources/vendor/jquery.cookie/jquery.cookie.js"></script>
    <script src="resources/vendor/chart.js/Chart.min.js"></script>
    <script src="resources/vendor/jquery-validation/jquery.validate.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="resources/js/front.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    
</body>
</html>