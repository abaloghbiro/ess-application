<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isErrorPage="true"%>

<!DOCTYPE html>
<html>
<head>
<title>BH Hallgatói portál</title>
<%@include file="header.jsp"%>
</head>
<body>
	<div class="page login-page">
		<div class="container d-flex align-items-center">
			<div class="form-holder has-shadow">
				<div class="row">
					<!-- Logo & Information Panel-->
					<div class="col-lg-12">
						<div class="info d-flex align-items-center">
							<div class="content">
								<h1>Általános hiba történt az oldal kiszolgálása közben :(</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copyrights text-center">
			<p>
				Design by <a href="https://bootstrapious.com/admin-templates"
					class="external">BootStrapious</a>
				<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
			</p>
		</div>
	</div>
	<%@include file="footer.jsp"%>
</body>
</html>