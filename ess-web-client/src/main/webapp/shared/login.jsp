<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="hu.braininghub.bh06.ess.dto.BhUserDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>BH portál - Bejelentkezés</title>
    <%@include file="header.jsp" %>
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>Braining Hub</h1>
                  </div>
                  <p>Portál</p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                  <form name="login" action="j_security_check" id="login-form" method="post">
                    <div class="form-group">
                      <input id="login-username" type="text" name="j_username" required="required" class="input-material">
                      <label for="login-username" class="label-material">Felhasználónév</label>
                    </div>
                    <div class="form-group">
                      <input id="login-password" type="password" name="j_password" required="required" class="input-material">
                      <label for="login-password" class="label-material">Jelszó</label>
                      <input type="hidden" name="userevent" value="login">
                    </div><input class="btn btn-primary" type="submit" id="login" value="Login">
                    <c:if test="${not empty fn:trim(message)}">
                    	<div class="alert alert-danger" role="alert"><c:out value= "${message}"/></div>
					</c:if>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">BootStrapious</a>
          <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
        </p>
      </div>
    </div>
    <%@include file="footer.jsp" %>
  </body>
</html>
