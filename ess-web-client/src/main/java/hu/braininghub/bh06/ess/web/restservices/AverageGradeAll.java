package hu.braininghub.bh06.ess.web.restservices;

import java.util.HashMap;
import java.util.Map;

//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//import hu.braininghub.bh06.ess.service.ExamService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

@Path("/averageAll")
public class AverageGradeAll {
	
	String proba = "proba1";
	Map<Integer, String> map = new HashMap<Integer, String>();
	
	
	
	//examService.getAllAverageExamResults()
	
//	@Inject
//	private ExamService examService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsersToJSON() throws BusinessObjectNotFoundException, DatabaseException {
		map.put(1, proba);
		map.put(2, "proba2");
		map.put(3, "proba3");
		return Response.status(Response.Status.OK).entity(map).build();

	}
	
	@Path(value = "/all")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getAllUsersToXML() throws BusinessObjectNotFoundException, DatabaseException {

		return Response.status(Response.Status.OK).entity(proba).build();

	}
	
	
	

}
