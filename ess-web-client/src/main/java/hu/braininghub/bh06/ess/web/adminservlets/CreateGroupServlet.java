package hu.braininghub.bh06.ess.web.adminservlets;

import java.io.IOException;
import java.security.InvalidParameterException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.service.GroupService;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.NameInUseException;

@WebServlet(urlPatterns = "/createGroup")
public class CreateGroupServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@EJB
	private GroupService groupService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("WEB-INF/admin/createGroup.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		BhGroupDTO newGroup = new BhGroupDTO();

		try {
			String groupName = req.getParameter("groupName");
			if (groupName == null) {
				throw new InvalidParameterException("The groupname field is empty!");
			} else {
				newGroup.setName(groupName);
				groupService.addNewGroup(newGroup);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		}

		req.getRequestDispatcher("/admin").forward(req, resp);
	}
}
