package hu.braininghub.bh06.ess.web.adminservlets;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.security.InvalidParameterException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.service.ExamService;

@WebServlet(urlPatterns = "/createExam")
public class CreateExamServlet extends HttpServlet {

	@EJB
	private ExamService examService;

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("WEB-INF/admin/createExam.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {

			if (req.getParameter("examName") != null && !"".equals(req.getParameter("examName"))
					&& !req.getParameter("examName").contains(" ") && req.getParameter("date") != null
					&& !"".equals(req.getParameter("date")) && !req.getParameter("date").contains(" ")
					&& req.getParameter("theoryPoint") != null && !"".equals(req.getParameter("theoryPoint"))
					&& !req.getParameter("theoryPoint").contains(" ") && req.getParameter("practicePoint") != null
					&& !"".equals(req.getParameter("practicePoint"))
					&& !req.getParameter("practicePoint").contains(" ")) {
				String examTitle = req.getParameter("examName");

				String examDate = req.getParameter("date");

				Double theoryPoint = Double.parseDouble(req.getParameter("theoryPoint"));
				BigDecimal maxScoreTheory = new BigDecimal(theoryPoint, MathContext.DECIMAL64);

				Double practicePoint = Double.parseDouble(req.getParameter("practicePoint"));
				BigDecimal maxScorePractice = new BigDecimal(practicePoint, MathContext.DECIMAL64);

				examService.createNewExam(examDate, examTitle, maxScoreTheory, maxScorePractice);

			} else {
				throw new InvalidParameterException("Some fields are empty!");
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		}

		req.getRequestDispatcher("WEB-INF/admin/admin.jsp").forward(req, resp);

	}
}
