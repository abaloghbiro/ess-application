package hu.braininghub.bh06.ess.web.restservices;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.service.GroupService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

@Path("/student")
public class DefaultStudentDataService implements StudentDataService {

	@EJB
	private GroupService groupService;

	@Override
	public List<BhUserDTO> getAllStudents() {

		List<BhUserDTO> allUsers = new ArrayList<>();

		try {
			List<BhGroupDTO> groups = groupService.getBhGroupDTOs();
			for (BhGroupDTO group : groups) {
				for (BhUserDTO user : group.getBhUserList()) {
					allUsers.add(user);
				}
			}
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return allUsers;
	}

	@Path(value = "/all")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsersToJSON() {

		return Response.status(Response.Status.OK).entity(getAllStudents()).build();

	}

	@Path(value = "/all")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getAllUsersToXML() {

		return Response.status(Response.Status.OK).entity(getAllStudents()).build();

	}

	@Override
	public BhGroupDTO getBhGroupDTOByName(String groupName) throws BusinessObjectNotFoundException {
		throw new UnsupportedOperationException("Not implemented yet!");
	}

}
