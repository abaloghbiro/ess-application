package hu.braininghub.bh06.ess.web.studentservlets;

import java.io.IOException;
import java.security.InvalidParameterException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.service.UserService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;



@WebServlet(urlPatterns = "/studentPassword")
public class StudentPasswordServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private UserService userService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		req.getRequestDispatcher("WEB-INF/student/changeStudentPassword.jsp").forward(req, resp);
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		req.setAttribute("success", false);
		
		try {
			BhUserDTO user = (BhUserDTO) req.getSession().getAttribute("user");
			String oldPassword = req.getParameter("oldpass");
			if (oldPassword != null && !"".equals(oldPassword) && !oldPassword.contains(" ") && user.getBhPass().equals(oldPassword)) {
				String newPassword1 = req.getParameter("newpass");
				String newPassword2 = req.getParameter("newpass2");
				if (newPassword1 != null && newPassword2 != null && !oldPassword.equals(newPassword1) && newPassword1.equals(newPassword2)) {
					userService.changeUserPassword(user.getUserName(), newPassword1);
					req.setAttribute("message", "A jelsz�v�ltozat�s sikeres volt!");
					req.setAttribute("success", true);
				}
				else {
					req.setAttribute("message", "A megadott jelsz� nem megfelel� form�tum�!");
				}
			}
			else {
				req.setAttribute("message", "A r�gi jelsz� nem megfelel�!");
			}
			
		} catch (InvalidParameterException e) {
			req.setAttribute("message", "A felhaszn�l� nem tal�lhat�!");
			e.printStackTrace();
		} catch (BusinessObjectNotFoundException e) {
			req.setAttribute("message", "A felhaszn�l� nem tal�lhat�!");
			e.printStackTrace();
		} catch (DatabaseException e) {
			req.setAttribute("message", "A felhaszn�l� nem tal�lhat�!");
			e.printStackTrace();
		}
		
		req.getRequestDispatcher("WEB-INF/student/changeStudentPassword.jsp").forward(req, resp);
		
//		} catch (BusinessObjectNotFoundException e) {
//			System.out.println(e.getLocalizedMessage());
//		}
		
	}
	
	
	
}
