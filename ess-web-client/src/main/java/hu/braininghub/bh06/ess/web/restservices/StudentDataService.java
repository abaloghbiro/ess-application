package hu.braininghub.bh06.ess.web.restservices;

import java.util.List;

import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;

public interface StudentDataService {
	
	public List<BhUserDTO> getAllStudents();
	
	public BhGroupDTO getBhGroupDTOByName(String groupName) throws BusinessObjectNotFoundException;
}
