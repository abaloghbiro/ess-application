package hu.braininghub.bh06.ess.web.instructorservlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.service.GroupService;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;


@WebServlet(urlPatterns = "/instructor")
public class InstructorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private GroupService groupService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		List<BhGroupDTO> groups = null;
		try {
			groups = groupService.getBhGroupDTOs();
		} catch (DatabaseException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		req.setAttribute("groups", groups);
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/instructor/instructor.jsp").forward(req, resp);
	}



	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			List<BhGroupDTO> allGroups = groupService.getBhGroupDTOs();
			req.setAttribute("groups", allGroups);
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/instructor/instructor.jsp").forward(req, resp);
		} catch (DatabaseException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		List<BhGroupDTO> allGroups = null;
		try {
			allGroups = groupService.getBhGroupDTOs();
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		req.setAttribute("groups", allGroups);
		req.getRequestDispatcher("/WEB-INF/instructor/instructor.jsp").forward(req, resp);
	}
	
	

}
