package hu.braininghub.bh06.ess.web.restservices;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.service.GroupService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

@Path("/studentGroup")
public class DefaultStudentDataGroup implements StudentDataService {

	private List<BhUserDTO> groupUsers;
	private BhGroupDTO group;
	private List<BhGroupDTO> groups;
	private GroupService groupService;

	@Override
	public List<BhUserDTO> getAllStudents() {
		try {
			groups = groupService.getBhGroupDTOs();

			for (BhGroupDTO group : groups) {
				for (BhUserDTO user : group.getBhUserList()) {
					groupUsers.add(user);
				}
			}
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return groupUsers;
	}

	@Override
	public BhGroupDTO getBhGroupDTOByName(String groupName) throws BusinessObjectNotFoundException {

		try {
			return groupService.getBhGroupDTOByName(groupName);
		} catch (DatabaseException e) {
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
		throw new RuntimeException("Group not found with the given name!" + groupName);
	}

	@GET
	@Path(value = "/{name}studentsByGroup")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupUsersToJSON(String groupName) throws BusinessObjectNotFoundException {
		group = getBhGroupDTOByName(groupName);
		for (BhUserDTO user : group.getBhUserList()) {
			groupUsers.add(user);
		}
		return Response.status(Response.Status.OK).entity(groupUsers).build();

	}

	@GET
	@Path(value = "/{name}studentsByGroup")
	@Produces(MediaType.APPLICATION_XML)
	public Response getGroupUsersToXML(String groupName) throws BusinessObjectNotFoundException {
		group = getBhGroupDTOByName(groupName);
		for (BhUserDTO user : group.getBhUserList()) {
			groupUsers.add(user);
		}
		return Response.status(Response.Status.OK).entity(groupUsers).build();

	}

}