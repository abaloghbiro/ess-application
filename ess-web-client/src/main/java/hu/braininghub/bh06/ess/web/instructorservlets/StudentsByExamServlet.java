package hu.braininghub.bh06.ess.web.instructorservlets;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.service.ExamService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;

@WebServlet(urlPatterns = "/studentsByExam")
public class StudentsByExamServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ExamService examService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//Map<Integer, List<String>> examDetails = new HashMap<>();
		
		/*List<String> testList1 = Arrays.asList("1", "2", "3", "4", "5", "6");
		List<String> testList2 = Arrays.asList("7", "8", "9", "10", "11", "12");
		
		examDetails.put(1, testList1);
		examDetails.put(2, testList2);*/
		
		String examId = req.getParameter("examId");
		String examName = req.getParameter("examName");
		
		/*req.setAttribute("examName", examName);
		req.setAttribute("examDetails", examDetails);
		req.getRequestDispatcher("WEB-INF/instructor/usersByExam.jsp").forward(req, resp);*/
		
		try {
			Map<Integer, List<String>> examDetails = examService.getExamDetailsByExam(Long.parseLong(examId));
			req.setAttribute("examDetails", examDetails);
			req.setAttribute("examName", examName);
			req.getRequestDispatcher("WEB-INF/instructor/usersByExam.jsp").forward(req, resp);
		} catch (NumberFormatException | BusinessObjectNotFoundException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
	}
	

}
