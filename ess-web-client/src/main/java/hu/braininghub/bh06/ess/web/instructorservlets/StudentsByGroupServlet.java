package hu.braininghub.bh06.ess.web.instructorservlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.service.GroupService;

@WebServlet(urlPatterns = "/studentsByGroup")
public class StudentsByGroupServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private GroupService groupService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String groupName = req.getParameter("groupName");

		BhGroupDTO group = null;
		try {
			group = groupService.getBhGroupDTOByName(groupName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		req.setAttribute("group", group);

		req.getRequestDispatcher("WEB-INF/instructor/instructorGroup.jsp").forward(req, resp);

	}

}
