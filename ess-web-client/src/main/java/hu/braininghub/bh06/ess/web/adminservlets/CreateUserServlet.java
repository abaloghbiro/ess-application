package hu.braininghub.bh06.ess.web.adminservlets;

import java.io.IOException;
import java.security.InvalidParameterException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;
import hu.braininghub.bh06.ess.service.GroupService;
import hu.braininghub.bh06.ess.service.UserService;

@WebServlet(urlPatterns = "/createUser")
public class CreateUserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private GroupService groupService;

	@EJB
	private UserService userService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("WEB-INF/admin/createUser.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String username = req.getParameter("username");
		String lastname = req.getParameter("lastname");
		String firstname = req.getParameter("firstname");
		String password = req.getParameter("password");
		String passwordAgain = req.getParameter("passwordAgain");
		String email = req.getParameter("email");
		String right = req.getParameter("right");
		String groupName = req.getParameter("group");

		try {

			if (username != null && !"".equals(username)
					&& !username.contains(" ") && lastname != null && !"".equals(lastname) && !lastname.contains(" ")
					&& firstname != null && !"".equals(firstname) && !firstname.contains(" ") && password != null
					&& !"".equals(password) && !password.contains(" ") && passwordAgain != null
					&& !"".equals(passwordAgain) && !passwordAgain.contains(" ") && password.equals(passwordAgain)
					&& email != null && !"".equals(email) && !email.contains(" ") && email.contains("@")) {

				userService.createNewUser(username, firstname, lastname, password, AccessLevels.valueOf(right),
						groupName, email);

			} else {
				throw new InvalidParameterException("Some fields are empty!");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		}
		resp.sendRedirect(req.getContextPath() + "/admin");
	}

}
