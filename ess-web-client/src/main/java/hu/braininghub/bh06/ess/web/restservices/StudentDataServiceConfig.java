package hu.braininghub.bh06.ess.web.restservices;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/studentData")
public class StudentDataServiceConfig extends Application {
	
	Set<Class<?>> studentData = new HashSet<>();

	@Override
	public Set<Class<?>> getClasses() {
		studentData.add(DefaultStudentDataService.class);
		return studentData;
	}
	

}
