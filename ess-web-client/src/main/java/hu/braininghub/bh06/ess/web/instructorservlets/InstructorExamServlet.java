package hu.braininghub.bh06.ess.web.instructorservlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.ExamDTO;
import hu.braininghub.bh06.ess.service.ExamService;
import hu.braininghub.bh06.ess.service.GroupService;

@WebServlet(urlPatterns = "/instructorExam")
public class InstructorExamServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ExamService examService;
	@EJB
	private GroupService groupService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String groupName = req.getParameter("groupName");

		BhGroupDTO currentGroup;

		try {
			currentGroup = groupService.getBhGroupDTOByName(groupName);
			List<ExamDTO> exams = examService.getExamsByGroup(currentGroup.getId());
			req.setAttribute("exams", exams);
			req.getRequestDispatcher("WEB-INF/instructor/instructorExams.jsp").forward(req, resp);
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

}
