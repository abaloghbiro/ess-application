package hu.braininghub.bh06.ess.web.adminservlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.dto.ExamDTO;
import hu.braininghub.bh06.ess.service.ExamService;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

@WebServlet(urlPatterns = "/adminExam")
public class AdminExamServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@EJB
	private ExamService examService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			List<ExamDTO> exams = examService.getAllExamDTO();
			req.setAttribute("exams", exams);
			req.getRequestDispatcher("WEB-INF/admin/exam.jsp").forward(req, resp);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("WEB-INF/admin/exam.jsp").forward(req, resp);
	}

}
