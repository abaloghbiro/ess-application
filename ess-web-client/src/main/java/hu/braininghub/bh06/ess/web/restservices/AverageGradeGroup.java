package hu.braininghub.bh06.ess.web.restservices;

//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//import hu.braininghub.bh06.ess.service.ExamService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

@Path("/averageGroup")
public class AverageGradeGroup {
	
	String proba = "proba1";
	
//	@Inject
//	private ExamService examService;
	
	@Path(value = "/{name}1")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupUsersToJSON(@PathParam("name") String groupName) throws BusinessObjectNotFoundException, DatabaseException {
		
		return Response.status(Response.Status.OK).entity(proba).build();

	}
	
	@Path(value = "/{name}2")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getGroupUsersToXML(@PathParam("name") String groupName) throws BusinessObjectNotFoundException, DatabaseException {
		
		return Response.status(Response.Status.OK).entity(groupName).build();

	}

}
