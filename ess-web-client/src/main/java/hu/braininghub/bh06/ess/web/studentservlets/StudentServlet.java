package hu.braininghub.bh06.ess.web.studentservlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;

@WebServlet(urlPatterns = "/student")
public class StudentServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		
		BhUserDTO user = (BhUserDTO) session.getAttribute("user");
		
		List<ExamAttendanceDTO> exams = user.getExamResultList();
		
		req.setAttribute("exams", exams);
		
		req.getRequestDispatcher("WEB-INF/student/student.jsp").forward(req, resp);
	}
	
	
	
}
