package hu.braininghub.bh06.ess.web.instructorservlets;

import java.io.IOException;
import java.security.InvalidParameterException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.dto.BhUserDTO;

@WebServlet(urlPatterns = "/instructorPassword")
public class InstructorPasswordServlet extends HttpServlet {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
						
		req.getRequestDispatcher("WEB-INF/instructor/changeInstructorPassword.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		req.setAttribute("success", false);
		
		try {
			BhUserDTO user = (BhUserDTO) req.getSession().getAttribute("user");
			req.setAttribute("message", "");
			String oldPassword = req.getParameter("oldpass");
			if (oldPassword != null && !"".equals(oldPassword) && !oldPassword.contains(" ") && user.getBhPass().equals(oldPassword)) {
				String newPassword1 = req.getParameter("newpass");
				String newPassword2 = req.getParameter("newpass2");
				if (newPassword1 != null && newPassword2 != null && !oldPassword.equals(newPassword1) && newPassword1.equals(newPassword2)) {
					req.setAttribute("message", "A jelsz�v�ltozat�s sikeres volt!");
					req.setAttribute("success", true);
				}
				else {
					req.setAttribute("message", "A megadott jelsz� nem megfelel� form�tum�!");
				}
			}
			else {
				req.setAttribute("message", "A r�gi jelsz� nem megfelel�!");
			}
			
		} catch (InvalidParameterException e) {
			req.setAttribute("message", "A felhaszn�l� nem tal�lhat�!");
			e.printStackTrace();
		}
//		} catch (BusinessObjectNotFoundException e) {
//			System.out.println(e.getLocalizedMessage());
//		}
		
		req.getRequestDispatcher("WEB-INF/instructor/changeInstructorPassword.jsp").forward(req, resp);
		
	}
		
}
