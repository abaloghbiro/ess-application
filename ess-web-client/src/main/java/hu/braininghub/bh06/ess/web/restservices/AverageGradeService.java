package hu.braininghub.bh06.ess.web.restservices;

import java.util.Map;

import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

public interface AverageGradeService {
	
	
	public Map<String, Map<String, Double>> getAllAverageExamResults() throws BusinessObjectNotFoundException, DatabaseException;
	
	public Map<String, Double> getAverageExamResultsByGroup(String groupName) throws BusinessObjectNotFoundException, DatabaseException;
	
}
