package hu.braininghub.bh06.ess.web.sharedservlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;

@WebServlet(urlPatterns = "/download")
public class DownloadServlet extends HttpServlet {

	/**
	 * CzB
	 */
	private static final long serialVersionUID = 1L;

	@Resource(mappedName = "downloadFilePath")
	private String uploadedFilePath;

	@Resource(mappedName = "uploadFilePath")
	private String downloadedFilePath;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String examTitle = request.getParameter("examTitle");
		String groupName = request.getParameter("groupName");


		ZipFile zipFile2Download;
		String zipFile2DownloadName = downloadedFilePath + groupName + "-" + examTitle + ".zip";


		File uploadedFiles = new File(uploadedFilePath);
		if (uploadedFiles.exists()) {
			String outPath = uploadedFilePath;
			String outFileName = groupName + ".zip";

			File folderTo = new File(outPath + "temp");

			folderTo.mkdir();
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(folderTo + outFileName));

			// Collect and extract files
			File[] directoryListing = uploadedFiles.listFiles();
			if (directoryListing != null) {
				for (File child : directoryListing) {
					String folderName = (folderTo + "\\" + child.getName()).substring(0,
							(folderTo + child.getName()).length() - 3);
					File folder = new File(folderName);
					folder.mkdir();
					try {
						ZipFile zipFile = new ZipFile(child.getAbsolutePath());
						zipFile.extractAll(folder.getAbsolutePath());
					} catch (ZipException e) {
						e.printStackTrace();
					}
				}
				try {
					ZipParameters parameters = new ZipParameters();
					parameters.setIncludeRootFolder(false);
					zipFile2Download = new ZipFile(zipFile2DownloadName);
					zipFile2Download.addFolder(folderTo, parameters);
				} catch (ZipException ex) {
					Logger.getLogger(DownloadServlet.class.getName()).log(Level.SEVERE, null, ex);
				}
			}

			out.close();

			// send MIME-type to the browser
			String fileType = "application/zip";
			response.setContentType(fileType);

			// pop-up download window
			response.setHeader("Content-disposition", "attachment; filename=" + outFileName);

			// output filename as saved to the server and goes to the browser
			File file2Download = new File(zipFile2DownloadName);

			// send the file to the browser
			OutputStream zipOut = response.getOutputStream();
			FileInputStream in = new FileInputStream(file2Download);
			byte[] buffer = new byte[4096];
			int length;
			while ((length = in.read(buffer)) > 0) {
				zipOut.write(buffer, 0, length);
			}
			in.close();
			zipOut.flush();

			// delete unused folders and contents
			FileUtils.deleteDirectory(folderTo);

		} else {
			response.sendError(0);
		}
		response.sendRedirect("index.jsp");
	}

}
