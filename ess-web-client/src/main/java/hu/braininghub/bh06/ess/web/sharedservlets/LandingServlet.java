package hu.braininghub.bh06.ess.web.sharedservlets;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.dto.RightDTO;
import hu.braininghub.bh06.ess.service.UserService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

@WebServlet(urlPatterns = "/landing")
public class LandingServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<AccessLevels, String> accessLevelRouter;

	@EJB
	private UserService userService;

	@Override
	public void init() {
		accessLevelRouter = new HashMap<>();
		accessLevelRouter.put(AccessLevels.STUDENT, "/student");
		accessLevelRouter.put(AccessLevels.INSTRUCTOR, "/instructor");
		accessLevelRouter.put(AccessLevels.ADMIN, "/admin");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HttpSession session = req.getSession();

		BhUserDTO user = null;

		try {

			Principal loggedInUser = req.getUserPrincipal();

			user = userService.getBhUserDTOByUsername(loggedInUser.getName());

			RightDTO rightDTO = user.getRight();
			String servletPath = accessLevelRouter.get(rightDTO.getAccessLevel());
			session.setAttribute("user", user);
			resp.sendRedirect(req.getContextPath() + servletPath);

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
