package hu.braininghub.bh06.ess.web.adminservlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.service.UserService;


@WebServlet(urlPatterns = "/admin")
public class AdminServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private UserService userService;
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		List<BhUserDTO> users = null;
		
		try {
			users = userService.getAllBhUserDTO();

		} catch (Exception e) {
			e.printStackTrace();
		}
		req.setAttribute("users", users);
		req.getRequestDispatcher("WEB-INF/admin/admin.jsp").forward(req, resp);
		
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("WEB-INF/admin/admin.jsp").forward(req, resp);
	}

}
