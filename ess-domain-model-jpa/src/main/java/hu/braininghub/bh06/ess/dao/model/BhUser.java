package hu.braininghub.bh06.ess.dao.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "BH_USER")
public class BhUser extends BusinessObject implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
	@Column(name = "USER_NAME", nullable = false, unique = true)
	private String userName;
	@Basic(optional = false)
	@Column(name = "FIRST_NAME", nullable = false)
	private String firstName;
	@Basic(optional = false)
	@Column(name = "LAST_NAME", nullable = false)
	private String lastName;
	@Basic(optional = false)
	@Column(name = "BH_PASS", nullable = false)
	private String bhPass;
	@Basic(optional = false)
	@Column(name = "ACTIVE", nullable = false)
	private Integer active;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private List<ExamAttendance> examResultList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
	private List<Session> sessionList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private List<ExamFile> examFileList;
	@JoinColumn(name = "GROUP_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(optional = false)
	private Groups group;
	@JoinColumn(name = "RIGHT_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(optional = false)
	private Right right;
	@Column(name = "EMAIL", nullable = true)
	private String emailAddress;

	public BhUser() {
		super();
	}

	public BhUser(String firstName, String lastName, String bhPass) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.bhPass = bhPass;
	}

	public BhUser(String userName, String firstName, String lastName, String bhPass, int active, Groups groupId,
			Right rightId) {
		super();
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.bhPass = bhPass;
		this.group = groupId;
		this.right = rightId;
		this.active = active;
	}

	public String getUsername() {
		return userName;
	}

	public void setUsername(String username) {
		this.userName = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBhPass() {
		return bhPass;
	}

	public void setBhPass(String bhPass) {
		this.bhPass = bhPass;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public List<ExamAttendance> getExamResultList() {
		return examResultList;
	}

	public void setExamResultList(List<ExamAttendance> examResultList) {
		this.examResultList = examResultList;
	}

	public List<Session> getSessionList() {
		return sessionList;
	}

	public void setSessionList(List<Session> sessionList) {
		this.sessionList = sessionList;
	}

	public List<ExamFile> getExamFileList() {
		return examFileList;
	}

	public void setExamFileList(List<ExamFile> examFileList) {
		this.examFileList = examFileList;
	}

	public Groups getGroup() {
		return group;
	}

	public void setGroup(Groups group) {
		this.group = group;
	}

	public Right getRight() {
		return right;
	}

	public void setRight(Right right) {
		this.right = right;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof BhUser)) {
			return false;
		}
		BhUser other = (BhUser) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "model.entities.BhUser[ id=" + id + " ]";
	}

}
