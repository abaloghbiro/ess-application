package hu.braininghub.bh06.ess.dao.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SESSIONS")
public class Session extends BusinessObject implements Serializable {

  private static final long serialVersionUID = 1L;
  @Basic(optional = false)
  @Column(name = "DT_START", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtStart;
  @Column(name = "DT_END")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtEnd;
  @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private BhUser userId;

  public Session() {
	  super();
  }

  public Session(Date dtStart) {
	  super();
    this.dtStart = dtStart;
  }

  public Date getDtStart() {
    return dtStart;
  }

  public void setDtStart(Date dtStart) {
    this.dtStart = dtStart;
  }

  public Date getDtEnd() {
    return dtEnd;
  }

  public void setDtEnd(Date dtEnd) {
    this.dtEnd = dtEnd;
  }

  public BhUser getUserId() {
    return userId;
  }

  public void setUserId(BhUser userId) {
    this.userId = userId;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Session)) {
      return false;
    }
    Session other = (Session) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Session{" + "id=" + id + ", dtStart=" + dtStart + ", dtEnd=" + dtEnd + ", userId=" + userId + '}';
  }
  
}
