package hu.braininghub.bh06.ess.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "EXAM_ATTENDANCES", uniqueConstraints={
    @UniqueConstraint(columnNames = {"USER_ID", "EXAM_ID"})
})
public class ExamAttendance extends BusinessObject implements Serializable {

  private static final long serialVersionUID = 1L;
  @Basic(optional = false)
  @Column(name = "SCORE_THEORY")
  private BigDecimal scoreTheory;
  @Basic(optional = false)
  @Column(name = "SCORE_PRACTICE")
  private BigDecimal scorePractice;
  @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private BhUser user;
  @JoinColumn(name = "EXAM_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private Exam exam;

  public ExamAttendance() {
	  super();
  }

  public ExamAttendance(BigDecimal scoreTheory, BigDecimal scorePractice) {
	super();
	this.scoreTheory = scoreTheory;
    this.scorePractice = scorePractice;
  }

  public ExamAttendance(BigDecimal scoreTheory, BigDecimal scorePractice, BhUser userId, Exam exam) {
    this.scoreTheory = scoreTheory;
    this.scorePractice = scorePractice;
    this.user = userId;
    this.exam = exam;
  }

  public BigDecimal getScoreTheory() {
    return scoreTheory;
  }

  public void setScoreTheory(BigDecimal scoreTheory) {
    this.scoreTheory = scoreTheory;
  }

  public BigDecimal getScorePractice() {
    return scorePractice;
  }

  public void setScorePractice(BigDecimal scorePractice) {
    this.scorePractice = scorePractice;
  }

  public BhUser getUser() {
    return user;
  }

  public void setUser(BhUser user) {
    this.user = user;
  }

  public Exam getExam() {
    return exam;
  }

  public void setExam(Exam exam) {
    this.exam = exam;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof ExamAttendance)) {
      return false;
    }
    ExamAttendance other = (ExamAttendance) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "model.entities.ExamResult[ id=" + id + " ]";
  }
  
}