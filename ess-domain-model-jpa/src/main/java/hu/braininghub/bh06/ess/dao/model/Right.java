/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh06.ess.dao.model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;

@Entity
@Table(name = "RIGHTS")
public class Right extends BusinessObject implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
	@Column(name = "ACCESS_LEVEL", unique = true, nullable = false)
	@Enumerated(EnumType.STRING)
	private AccessLevels accessLevel;
	@Basic(optional = false)
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "right")
	private List<BhUser> bhUserList = new ArrayList<>();
	public Right() {
		super();
	}

	public Right(AccessLevels accessLevel) {
		super();
		this.accessLevel = accessLevel;
	}
	

	public AccessLevels getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(AccessLevels accessLevel) {
		this.accessLevel = accessLevel;
	}

	public List<BhUser> getBhUserList() {
		return bhUserList;
	}

	public void setBhUserList(List<BhUser> bhUserList) {
		this.bhUserList = bhUserList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Right)) {
			return false;
		}
		Right other = (Right) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "model.entities.Right[ id=" + id + ", accessLevel=" + accessLevel + ", accessLevelName="
				+ accessLevel.name() + " ]";
	}

}
