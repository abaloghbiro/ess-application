package hu.braininghub.bh06.ess.service.exception;

import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;

public class GroupNotFoundException extends BusinessObjectNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GroupNotFoundException() {
		super("Group not found");
	}

}
