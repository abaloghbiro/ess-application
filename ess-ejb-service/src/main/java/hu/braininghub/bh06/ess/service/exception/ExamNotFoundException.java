package hu.braininghub.bh06.ess.service.exception;

import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;

public class ExamNotFoundException extends BusinessObjectNotFoundException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExamNotFoundException() {
		super("Exam not found");
	}

}
