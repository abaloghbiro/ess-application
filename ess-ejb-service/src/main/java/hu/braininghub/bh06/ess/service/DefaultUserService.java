
package hu.braininghub.bh06.ess.service;

import static hu.braininghub.bh06.ess.common.auth.AuthenticationUtils.encodeSHA256;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;
import hu.braininghub.bh06.ess.dao.BhGroupDAO;
import hu.braininghub.bh06.ess.dao.BhUserDAO;
import hu.braininghub.bh06.ess.dao.ExamDAO;
import hu.braininghub.bh06.ess.dao.RightDAO;
import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;
import hu.braininghub.bh06.ess.dto.ExamDTO;
import hu.braininghub.bh06.ess.dto.RightDTO;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.ExamNotFoundException;
import hu.braininghub.bh06.ess.service.exception.NameInUseException;
import hu.braininghub.bh06.ess.service.exception.UserNotFoundException;
import hu.braininghub.bh06.ess.service.exception.UsernameInUseException;

@Stateless
@DeclareRoles({ "student", "instructor", "admin" })
public class DefaultUserService implements UserService {

	@Inject
	private BhUserDAO bhUserDao;

	@Inject
	private RightDAO rightDao;

	@Inject
	private ExamDAO examDAO;

	@Inject
	private BhGroupDAO groupDAO;

	@Transactional(rollbackOn = Exception.class)
	@PermitAll
	@Override
	public void changeUserPassword(String username, String newPassword) throws DatabaseException {

		try {
			Optional<BhUserDTO> user = bhUserDao.getByUserName(username);

			if (user.isPresent()) {
				user.get().setBhPass(newPassword);
				try {
					bhUserDao.update(user.get());
				} catch (RuntimeException e) {
					System.err.println(e.getMessage());
					throw new DatabaseException();
				}
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
			throw new DatabaseException(e.getMessage());
		}
	}

	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin" })
	@Override
	public void createNewUser(BhUserDTO bhUserDTO) throws UsernameInUseException, DatabaseException {

		if (bhUserDao.getById(bhUserDTO.getId()) == null) {
			try {
				bhUserDao.insert(bhUserDTO);
			} catch (RuntimeException e) {
				throw new DatabaseException();
			}
		} else {
			throw new UsernameInUseException();
		}
	}

	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin" })
	@Override
	public void createNewUser(String userName, String firstName, String lastName, String password,
			AccessLevels accessLevel, String groupName, String email) throws NameInUseException, DatabaseException {

		try {

			Optional<BhUserDTO> user = bhUserDao.getByUserName(userName);

			if (!user.isPresent()) {
				BhUserDTO newUser = new BhUserDTO();

				newUser.setUserName(userName);
				newUser.setFirstName(firstName);
				newUser.setLastName(lastName);
				newUser.setBhPass(encodeSHA256(password));
				newUser.setEmailAddress(email);
				RightDTO right = rightDao.getByAccessLevels(accessLevel);
				newUser.setRight(right);
				newUser.setActive(1);

				BhGroupDTO group = groupDAO.getByName(groupName);
				newUser.setGroup(group);

				bhUserDao.insert(newUser);
			} else {
				throw new DatabaseException("User already exists with the given name!");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public List<BhUserDTO> getAllBhUserDTO() throws DatabaseException {

		List<BhUserDTO> allBhUsers = bhUserDao.getAll();

		if (allBhUsers != null) {
			return allBhUsers;
		} else {
			throw new DatabaseException();
		}
	}

	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public List<BhUserDTO> getAllStudents() {

		List<BhUserDTO> allUsers = bhUserDao.getAll();
		List<BhUserDTO> allStudent = new ArrayList<>();

		for (BhUserDTO bhUserDTO : allUsers) {
			if (bhUserDTO.getRight().getAccessLevel().equals(AccessLevels.STUDENT)) {
				allStudent.add(bhUserDTO);
			}
		}
		return allStudent;
	}

	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public BhUserDTO getBhUserDTOById(Long id) throws DatabaseException {

		try {
			BhUserDTO bhUserDTO = bhUserDao.getById(id);

			return bhUserDTO;

		} catch (RuntimeException e) {
			throw new DatabaseException();
		}
	}

	@Transactional
	@RolesAllowed({ "admin", "instructor", "student" })
	@Override
	public BhUserDTO getBhUserDTOByUsername(String userName) throws DatabaseException {

		Optional<BhUserDTO> bhUserDTO = bhUserDao.getByUserName(userName);

		if (!bhUserDTO.isPresent()) {
			throw new DatabaseException("User not found with the given name = > " + userName);
		}

		return bhUserDTO.get();

	}

	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public List<BhUserDTO> getStudentsByExam(Long examID) throws BusinessObjectNotFoundException, DatabaseException {

		try {
			ExamDTO exam = examDAO.getById(examID);

			if (exam == null)
				throw new ExamNotFoundException();

			List<ExamAttendanceDTO> results = exam.getExamResultList();

			Map<Long, BhUserDTO> users = new HashMap<>();

			BhUserDTO currentUser;

			for (ExamAttendanceDTO currentResult : results) {

				currentUser = currentResult.getUser();
				users.put(currentUser.getId(), currentUser);

			}

			return new ArrayList<BhUserDTO>(users.values());

		} catch (RuntimeException e) {
			throw new DatabaseException();
		}
	}

	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin" })
	@Override
	public void updateBhUser(BhUserDTO bhUserDTO) throws DatabaseException, UserNotFoundException {

		Long parameterID = bhUserDTO.getId();

		if (bhUserDao.getById(parameterID) != null) {
			try {
				bhUserDao.update(bhUserDTO);
			} catch (Exception e) {
				throw new DatabaseException();
			}
		} else
			throw new UserNotFoundException();
	}

}
