package hu.braininghub.bh06.ess.service.event;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;

import hu.braininghub.bh06.ess.dto.EmailDTO;
import hu.braininghub.bh06.ess.emailservice.EmailService;
import hu.braininghub.bh06.ess.event.EmailEventObserver;

@Named
@ApplicationScoped
public class DefaultEmailEventObserver implements EmailEventObserver {

	@Inject
	EmailService emailService;
	
	public void observeEmail(@Observes EmailDTO emailDTO) {
		emailService.sendEmail(emailDTO.getTo(), emailDTO.getSubject(), emailDTO.getBody());
	}
}
