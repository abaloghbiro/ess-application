package hu.braininghub.bh06.ess.service.exception;

import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;

public class ExamAttendanceNotFoundException extends BusinessObjectNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExamAttendanceNotFoundException() {
		super("ExamAttendance not found");
	}


}
