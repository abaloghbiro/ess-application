
package hu.braininghub.bh06.ess.service;

import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import hu.braininghub.bh06.ess.dao.BhGroupDAO;
import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.GroupNameInUseException;
import hu.braininghub.bh06.ess.service.exception.GroupNotFoundException;

@Stateless
@DeclareRoles({ "student", "instructor", "admin" })
public class DefaultGroupService implements GroupService {

	@Inject
	private BhGroupDAO groupDao;

	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public void addNewGroup(BhGroupDTO bhGroupDTO) throws DatabaseException, GroupNameInUseException {

		try {
			groupDao.insert(bhGroupDTO);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public BhGroupDTO getBhGroupDTOById(Long id) throws GroupNotFoundException {

		try {

			BhGroupDTO groupDto = groupDao.getById(id);

			return groupDto;

		} catch (Exception e) {
			throw new GroupNotFoundException();
		}
	}

	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public BhGroupDTO getBhGroupDTOByName(String groupName) throws DatabaseException {

		BhGroupDTO groupDTO;
		try {
			groupDTO = groupDao.getByName(groupName);
			return groupDTO;
		} catch (Exception e) {
			throw new DatabaseException();
		}

	}

	@Transactional
	@PermitAll
	@Override
	public List<BhGroupDTO> getBhGroupDTOs() throws DatabaseException {

		try {
			List<BhGroupDTO> groupDTOList = groupDao.getAll();
			return groupDTOList;
		} catch (Exception e) {
			throw new DatabaseException();
		}

	}

	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin", "instructor"})
	@Override
	public void updateBhGroup(BhGroupDTO bhGroupDTO) throws DatabaseException {

		try {
			groupDao.update(bhGroupDTO);
		} catch (Exception e) {
			throw new DatabaseException();
		}
	}

}
