package hu.braininghub.bh06.ess.service.exception;

import hu.braininghub.bh06.ess.service.exception.NameInUseException;

public class UsernameInUseException extends NameInUseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UsernameInUseException() {
		super("The username supplied is already in use");
	}

}
