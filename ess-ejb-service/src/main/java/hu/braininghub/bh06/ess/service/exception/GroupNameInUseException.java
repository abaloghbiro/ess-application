package hu.braininghub.bh06.ess.service.exception;

import hu.braininghub.bh06.ess.service.exception.NameInUseException;

public class GroupNameInUseException extends NameInUseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GroupNameInUseException() {
		super("Group name already in use");
		
	}

}
