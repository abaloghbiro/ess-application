package hu.braininghub.bh06.ess.service.exception;

public class ExamFileNotFoundException extends BusinessObjectNotFoundException {

	public ExamFileNotFoundException() {
		
		super("Could not find exam file.");
		
	}

}
