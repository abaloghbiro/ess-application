package hu.braininghub.bh06.ess.service;

import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import hu.braininghub.bh06.ess.dao.ExamFileDAO;
import hu.braininghub.bh06.ess.dto.ExamFileDTO;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

@Stateless
@DeclareRoles({ "student", "instructor", "admin" })
public class DefaultExamFileService implements ExamFileService {

	@Inject
	ExamFileDAO examFileDAO;

	@Transactional(rollbackOn = Exception.class)
	@PermitAll
	@Override
	public void addNewExamFile(ExamFileDTO examFileDTO) throws DatabaseException {

		try {
			examFileDAO.insert(examFileDTO);
		} catch (Exception e) {
			throw new DatabaseException();
		}

	}
	
	@Transactional
	@RolesAllowed({ "admin", "instructor", "student" })
	@Override
	public List<ExamFileDTO> getAllExamFileDTO() throws DatabaseException {

		List<ExamFileDTO> examFileList;

		try {
			examFileList = examFileDAO.getAll();

				return examFileList;

		} catch (Exception e) {
			throw new DatabaseException();
		}
	}
	
	@Transactional
	@PermitAll
	@Override
	public ExamFileDTO getExamFileDTO(Long id) throws DatabaseException {

		try {
			ExamFileDTO examFileDTO = examFileDAO.getById(id);

				return examFileDTO;

		} catch (RuntimeException e) {
			throw new DatabaseException();
		}

	}

	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin", "student", "instructor"})
	@Override
	public void updateExamFile(ExamFileDTO examFileDTO) throws DatabaseException {

		try {
			examFileDAO.update(examFileDTO);
		} catch (Exception e) {
			throw new DatabaseException();
		}
	}

	

}
