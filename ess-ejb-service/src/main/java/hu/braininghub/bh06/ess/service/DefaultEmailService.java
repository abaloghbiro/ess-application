package hu.braininghub.bh06.ess.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import hu.braininghub.bh06.ess.emailservice.EmailService;

@Stateless
public class DefaultEmailService implements EmailService {

	private Properties prop;
	private InputStream input;
	private Authenticator authenticator;
	private Session session;

	@Override
	public void sendEmail(String to, String subject, String body) {

		try {
			input = DefaultEmailService.class.getResourceAsStream("mailserver.properties");
			prop = new Properties();
			prop.load(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		authenticator = new Authenticator() {
			private PasswordAuthentication pa = new PasswordAuthentication(prop.getProperty("username"),
					prop.getProperty("password"));

			public PasswordAuthentication getPasswordAuthentication() {
				return pa;
			}
		};
		session = Session.getInstance(prop, authenticator);

		MimeMessage message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(prop.getProperty("from")));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setText(body);
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}
