
package hu.braininghub.bh06.ess.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import hu.braininghub.bh06.ess.dao.BhGroupDAO;
import hu.braininghub.bh06.ess.dao.BhUserDAO;
import hu.braininghub.bh06.ess.dao.ExamAttendanceDAO;
import hu.braininghub.bh06.ess.dao.ExamDAO;
import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.dto.EmailDTO;
import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;
import hu.braininghub.bh06.ess.dto.ExamDTO;
import hu.braininghub.bh06.ess.event.EmailEventSender;
import hu.braininghub.bh06.ess.service.ExamService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.ExamAttendanceNotFoundException;
import hu.braininghub.bh06.ess.service.exception.ExamNotFoundException;
import hu.braininghub.bh06.ess.service.exception.GroupNotFoundException;

/***
 * 
 * @author balazs
 * 
 */
@Stateless
@DeclareRoles({ "student", "instructor", "admin" })
public class DefaultExamService implements ExamService {

	@Inject
	private ExamAttendanceDAO examAttendanceDao;

	@Inject
	private ExamDAO examDao;

	@Inject
	private BhGroupDAO groupDao;

	@Inject
	private BhUserDAO userDAO;

	@Inject
	private EmailEventSender emailEventSender;

	/**
	 * Roles allowed: admin
	 */
	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin" })
	@Override
	public void addNewExam(ExamDTO examDTO) throws DatabaseException {

		try {
			examDao.insert(examDTO);
		} catch (Exception e) {
			throw new DatabaseException("There was an error with the database!");
		}

	}

	private Double averageExamResult(ExamDTO examDTO) {
		double average = 0;
		int sampleNumber = 0;
		for (ExamAttendanceDTO currentExamAttendance : examDTO.getExamResultList()) {
			if (currentExamAttendance.getScorePractice() != null && currentExamAttendance.getScoreTheory() != null) {
				average += (currentExamAttendance.getScorePractice().doubleValue()
						+ currentExamAttendance.getScoreTheory().doubleValue())
						/ (examDTO.getMaxScorePractice().doubleValue() + examDTO.getMaxScoreTheory().doubleValue());
				sampleNumber++;
			}
		}
		if (sampleNumber > 0) {
			average = average * 100 / sampleNumber;
		}

		return average;
	}

	/****
	 * Creates a ne The String examDate must have the date in the
	 * "yyyy-MM-dd-hh-mm-ss" format. Roles allowed: admin
	 * 
	 * @throws ParseException
	 * @throws DatabaseException
	 */
	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin" })
	@Override
	public void createNewExam(String examDate, String examTitle, BigDecimal maxScoreTheory, BigDecimal maxScorePractice)
			throws ParseException, DatabaseException {

		ExamDTO newExam = new ExamDTO();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");

		newExam.setExamDate(format.parse(examDate));
		newExam.setExamTitle(examTitle);
		newExam.setMaxScoreTheory(maxScoreTheory);
		newExam.setMaxScorePractice(maxScorePractice);

		addNewExam(newExam);
	}

	/***
	 * Returns <GroupName , <ExamName, percentageAverage>> Roles allowed: admin,
	 * instructor
	 * 
	 * @throws BusinessObjectNotFoundException
	 * @throws DatabaseException
	 */
	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public Map<String, Map<String, Double>> getAllAverageExamResults() throws DatabaseException {

		try {
			List<BhGroupDTO> groups = groupDao.getAll();

			Map<String, Map<String, Double>> map = new HashMap<>();

			for (BhGroupDTO currentGroup : groups) {
				map.put(currentGroup.getName(), calculateAverageExamResultsForGroup(currentGroup));
			}

			return map;
		} catch (RuntimeException e) {
			throw new DatabaseException();
		}
	}

	/***
	 * 
	 * Roles allowed: admin, instructor
	 * 
	 * @throws DatabaseException
	 */
	@Transactional
	@RolesAllowed({ "instructor", "admin" })
	@Override
	public List<ExamAttendanceDTO> getAllExamAttendanceDTO() throws DatabaseException {

		try {
			List<ExamAttendanceDTO> examAttendanceList = examAttendanceDao.getAll();

			if (examAttendanceList == null) {
				throw new DatabaseException();
			} else
				return examAttendanceList;
		} catch (RuntimeException e) {
			throw new DatabaseException("There was an error in the database!");
		}
	}

	/***
	 * Returns all the ExamDTO-s contained in the database. Roles allowed: admin,
	 * instructor
	 * 
	 * @throws DatabaseException
	 */
	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public List<ExamDTO> getAllExamDTO() throws DatabaseException {

		try {
			List<ExamDTO> examList = examDao.getAll();

			if (examList == null) {
				throw new DatabaseException();
			} else
				return examList;
		} catch (Exception e) {
			throw new DatabaseException();
		}
	}

	/**
	 * Returns a HashMap containing the usernames as keys and a list containing the
	 * average result per exam.
	 * 
	 * @throws BusinessObjectNotFoundException
	 * @throws DatabaseException
	 */
	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public Map<String, Double> getAverageExamResultsByGroup(String groupName)
			throws  DatabaseException {

		try {
			BhGroupDTO group = groupDao.getByName(groupName);
			return calculateAverageExamResultsForGroup(group);
		} catch (RuntimeException e) {
			throw new DatabaseException("Could not get group " + groupName + " from database!");
		}

	}

	private Map<String, Double> calculateAverageExamResultsForGroup(BhGroupDTO group) {

		List<ExamDTO> exams = listExamsForGroup(group);

		Map<String, Double> map = new HashMap<>();

		for (ExamDTO currentExam : exams) {
			map.put(currentExam.getExamTitle(), averageExamResult(currentExam));
		}

		return map;
	}

	/***
	 * Roles allowed: admin, instructor
	 * 
	 * @throws DatabaseException
	 * @throws BusinessObjectNotFoundException
	 */
	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public ExamAttendanceDTO getExamAttendanceDTO(Long id) throws  DatabaseException {

		try {
			ExamAttendanceDTO dto = examAttendanceDao.getById(id);
			return dto;

		} catch (Exception e) {
			throw new DatabaseException();
		}
	}

	/**
	 * Roles allowed: admin, instructor
	 * 
	 * @throws BusinessObjectNotFoundException
	 */
	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public Map<Integer, List<String>> getExamDetailsByExam(Long examId) throws BusinessObjectNotFoundException {

		try {
			ExamDTO exam = examDao.getById(examId);

			List<ExamAttendanceDTO> examAttendances = exam.getExamResultList();

			Map<Integer, List<String>> examDetails = new HashMap<>();

			for (int i = 0; i < examAttendances.size(); i++) {

				ExamAttendanceDTO currentExamAttendance = examAttendances.get(i);

				List<String> currentLine = new ArrayList<>();

				currentLine.add(currentExamAttendance.getUser().getFirstName());
				currentLine.add(currentExamAttendance.getUser().getLastName());
				currentLine.add(currentExamAttendance.getUser().getGroup().getName());
				currentLine.add(currentExamAttendance.getUser().getUserName());
				if (currentExamAttendance.getScorePractice() != null && currentExamAttendance.getScoreTheory() != null) {
					currentLine.add(String.valueOf(currentExamAttendance.getScorePractice()));
					currentLine.add(
							String.format(Locale.ENGLISH, "%6.2f", ((currentExamAttendance.getScorePractice().doubleValue() * 100.0)
									/ exam.getMaxScorePractice().doubleValue())) + " %");
					currentLine.add(String.valueOf(currentExamAttendance.getScoreTheory()));
					currentLine.add(String.format(Locale.ENGLISH, "%6.2f",
							((currentExamAttendance.getScoreTheory().doubleValue() * 100.0) / exam.getMaxScoreTheory().doubleValue()))
							+ " %");
					currentLine.add(String.format(Locale.ENGLISH, "%6.2f",
							(currentExamAttendance.getScorePractice().doubleValue()
									+ currentExamAttendance.getScoreTheory().doubleValue()) * 100
									/ (exam.getMaxScorePractice().doubleValue() + exam.getMaxScoreTheory().doubleValue()))
							+ " %");
				} else {
					currentLine.add("N/A");
					currentLine.add("N/A");
					currentLine.add("N/A");
					currentLine.add("N/A");
					currentLine.add("N/A");
				}

				examDetails.put(i, currentLine);
			}
			return examDetails;
		} catch (RuntimeException e) {
			throw new ExamNotFoundException();
		}
	}

	/**
	 * Roles allowed: admin, instructor
	 */
	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public Map<Integer, List<String>> getExamDetailsByUser(BhUserDTO user) {

		List<ExamAttendanceDTO> examAttendances = user.getExamResultList();
		Map<Integer, List<String>> examDetails = new HashMap<>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");

		for (int i = 0; i < examAttendances.size(); i++) {

			ExamAttendanceDTO currentExamAttendance = examAttendances.get(i);

			ExamDTO currentExam = currentExamAttendance.getExam();

			List<String> currentLine = new ArrayList<>();

			currentLine.add(currentExamAttendance.getScorePractice().toString());
			currentLine
					.add(String.format(Locale.ENGLISH, "%6.2f", ((currentExamAttendance.getScorePractice().doubleValue() * 100.0)
							/ currentExam.getMaxScorePractice().doubleValue())) + " %");
			currentLine.add(currentExamAttendance.getScoreTheory().toString());
			currentLine
					.add(String.format(Locale.ENGLISH, "%6.2f", ((currentExamAttendance.getScoreTheory().doubleValue() * 100.0)
							/ currentExam.getMaxScoreTheory().doubleValue())) + " %");
			currentLine.add(dateFormat.format(currentExam.getExamDate()));

			examDetails.put(i, currentLine);
		}
		return examDetails;

	}

	/**
	 * Roles allowed: admin, instructor
	 */
	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public ExamDTO getExamDTO(Long id) throws  DatabaseException {

		try {
			ExamDTO examDTO = examDao.getById(id);


				return examDTO;

		} catch (Exception e) {
			throw new DatabaseException();
		}

	}

	/**
	 * Roles allowed: admin, instructor
	 * 
	 * @throws GroupNotFoundException
	 */
	@Transactional
	@RolesAllowed({ "admin", "instructor" })
	@Override
	public List<ExamDTO> getExamsByGroup(Long groupId) throws DatabaseException {

		try {
			BhGroupDTO group = groupDao.getById(groupId);
			return listExamsForGroup(group);
		} catch (Exception e) {
			throw new DatabaseException();
		}

	}

	private ArrayList<ExamDTO> listExamsForGroup(BhGroupDTO group) {
		List<BhUserDTO> userList = group.getBhUserList();
		Map<Long, ExamDTO> exams = new HashMap<>();
		for (BhUserDTO bhUserDTO : userList) {
			for (ExamAttendanceDTO examAttendance : bhUserDTO.getExamResultList()) {
				exams.put(examAttendance.getExam().getId(), examAttendance.getExam());
			}
		}
		return new ArrayList<ExamDTO>(exams.values());
	}

	/**
	 * Roles allowed: admin
	 * 
	 * @throws DatabaseException
	 * 
	 * @throws GroupNotFoundException
	 * @throws ExamAttendanceNotFoundException
	 */
	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin" })
	@Override
	public void linkExamToGroup(Long examId, Long groupId) throws BusinessObjectNotFoundException, DatabaseException {

		try {
			ExamDTO exam = examDao.getById(examId);

			try {
				BhGroupDTO group = groupDao.getById(groupId);

				for (BhUserDTO currentUser : group.getBhUserList()) {

					ExamAttendanceDTO currentExamAttendace = new ExamAttendanceDTO();

					currentExamAttendace.setExam(exam);
					currentExamAttendace.setUser(currentUser);

					currentUser.getExamResultList().add(currentExamAttendace);

					try {
						userDAO.update(currentUser);
					} catch (Exception e) {
						throw new DatabaseException();
					}
				}

			} catch (RuntimeException e) {
				throw new GroupNotFoundException();
			}
		} catch (RuntimeException e) {
			throw new ExamNotFoundException();
		}
	}

	/**
	 * 
	 * Roles allowed: admin
	 * 
	 * @throws DatabaseException
	 */
	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin" })
	@Override
	public void updateExam(ExamDTO examDTO) throws DatabaseException {

		try {
			examDao.update(examDTO);
		} catch (Exception e) {
			throw new DatabaseException();
		}

	}

	/**
	 * Roles allowed: admin
	 * 
	 * @throws DatabaseException
	 */
	@Transactional(rollbackOn = Exception.class)
	@RolesAllowed({ "admin" })
	@Override
	public void updateExamAttendance(ExamAttendanceDTO examAttendanceDTO) throws DatabaseException {

		try {
			examAttendanceDao.update(examAttendanceDTO);
			if (examAttendanceDTO.getScorePractice() != null && examAttendanceDTO.getScoreTheory() != null
					&& examAttendanceDTO.getUser() != null) {
				String to = examAttendanceDTO.getUser().getEmailAddress();
				String subject = "Your exam results";
				String body = "Theory: " + examAttendanceDTO.getScoreTheory() + "\nPractice: "
						+ examAttendanceDTO.getScorePractice();

				EmailDTO emailDTO = new EmailDTO(to, subject, body);
				emailEventSender.sendEmail(emailDTO);
			}
		} catch (Exception e) {
			throw new DatabaseException();
		}

	}

}
