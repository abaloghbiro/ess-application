package hu.braininghub.bh06.ess.service.event;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import hu.braininghub.bh06.ess.dto.EmailDTO;
import hu.braininghub.bh06.ess.event.EmailEventSender;

@Named
@RequestScoped
public class DefaultEmailEventSender implements EmailEventSender {

	@Inject
	private Event<EmailDTO> emailEvent;
	
	public void sendEmail(EmailDTO emailDTO) {
		emailEvent.fire(emailDTO);
	}	
	
	
}
