package hu.braininghub.bh06.ess.service.exception;

import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;

public class UserNotFoundException extends BusinessObjectNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotFoundException() {
		super("Could not locate user with the given name!");
	}
	
}
