package hu.braininghub.bh06.ejb.service.test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import hu.braininghub.bh06.ess.dao.BhGroupDAO;
import hu.braininghub.bh06.ess.dao.BhUserDAO;
import hu.braininghub.bh06.ess.dao.ExamAttendanceDAO;
import hu.braininghub.bh06.ess.dao.ExamDAO;
import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.service.DefaultGroupService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.GroupNameInUseException;
import hu.braininghub.bh06.ess.service.exception.GroupNotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {

	@Mock BhUserDAO userDAO;
	@Mock	BhGroupDAO groupDAO;
	@Mock ExamDAO examDAO;
	@Mock ExamAttendanceDAO examAttendanceDAO;

	
	@InjectMocks private DefaultGroupService groupService;
	
	BhGroupDTO groupDTO1; 
	BhGroupDTO groupDTO2;
		
	@Before
	public void init() {
		groupDTO1 = new BhGroupDTO();
		groupDTO1.setName("BH05");
		groupDTO2	 = new BhGroupDTO(); 
		groupDTO2.setName("BH06");	
		
//		TestDummyValues dummyValues = new TestDummyValues();
//		
//		for(BhGroupDTO current : dummyValues.allGroupDTOs) {
//			Mockito.when(groupDAO.getById(current.getId())).thenReturn(current);
//		}
	}
	
	@Test
	public final void testAddNewGroupWorks() {
		
		try {
			groupService.addNewGroup(groupDTO1);
		} catch (GroupNameInUseException | DatabaseException e) {

			e.printStackTrace();
		}
	}
	
	@Test(expected = RuntimeException.class)
	public final void testAddNewGroupFailsBecauseNameExists() throws GroupNameInUseException, DatabaseException {
		doThrow(new RuntimeException()).when(groupDAO).insert(groupDTO2);
		groupService.addNewGroup(groupDTO2);

	}
	
	
	@Test
	public final void testGetBhGroupDTOByIdWorks() throws GroupNotFoundException  {
		when(groupDAO.getById(25L)).thenReturn(groupDTO2);

		BhGroupDTO newGroupDTO = groupService.getBhGroupDTOById(25L);
		assertNotNull(newGroupDTO);
		assertEquals(newGroupDTO, groupDTO2);

	}
	

	@Test(expected = GroupNotFoundException.class)
	public final void testGetBhGroupDTOByIdThrowsException() throws GroupNotFoundException {
		when(groupDAO.getById(25L)).thenThrow(new RuntimeException());
		groupService.getBhGroupDTOById(25L);
		
	}
	
	@Test public final void testGetBhGroupDTOByName() throws DatabaseException{
		when(groupDAO.getByName("BH05")).thenReturn(groupDTO1);
		
		BhGroupDTO newGroupDTO = groupService.getBhGroupDTOByName("BH05");
		assertNotNull(newGroupDTO);
		assertEquals(newGroupDTO, groupDTO1);
		
	}
	
	@Test(expected = DatabaseException.class)
	public final void testGetBhGroupDTOByNameFailsFromDAO() throws DatabaseException{
		when(groupDAO.getByName("BH05")).thenThrow(RuntimeException.class);
		
		groupService.getBhGroupDTOByName("BH05");
		
	} 
	
	@Test public final void testGetAllBhGroupDTOs() throws DatabaseException {
		
		List<BhGroupDTO> groupDTOList = groupService.getBhGroupDTOs();
		assertNotNull(groupDTOList);
		assertEquals(groupDAO.getAll().size(), groupDTOList.size()); 

	}
	
	@Test(expected = DatabaseException.class)
	public final void testAllBhGroupFailsFromDAO() throws DatabaseException {
			
		when(groupDAO.getAll()).thenThrow(RuntimeException.class);
		
		groupService.getBhGroupDTOs(); 
	}
	
	@Test public final void testUpdateAGroup() throws DatabaseException{
		
		groupDTO1.setName("new");
		groupService.updateBhGroup(groupDTO1);
		
		assertEquals("new", groupDTO1.getName());
	
	}
	
	@Test (expected = DatabaseException.class)
	public final void testUpdateAGroupFailsFromDAO() throws DatabaseException, BusinessObjectNotFoundException {
			
		doThrow(RuntimeException.class).when(groupDAO).update(groupDTO1);
		
		groupService.updateBhGroup(groupDTO1);
	}
}
