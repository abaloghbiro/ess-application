package hu.braininghub.bh06.ejb.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import hu.braininghub.bh06.ess.dao.BhGroupDAO;
import hu.braininghub.bh06.ess.dao.BhUserDAO;
import hu.braininghub.bh06.ess.dao.ExamAttendanceDAO;
import hu.braininghub.bh06.ess.dao.ExamDAO;
import hu.braininghub.bh06.ess.dao.ExamFileDAO;
import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.dto.EmailDTO;
import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;
import hu.braininghub.bh06.ess.dto.ExamDTO;
import hu.braininghub.bh06.ess.dto.ExamFileDTO;
import hu.braininghub.bh06.ess.event.EmailEventSender;
import hu.braininghub.bh06.ess.service.DefaultExamService;
import hu.braininghub.bh06.ess.service.event.DefaultEmailEventSender;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.ExamNotFoundException;
import hu.braininghub.bh06.ess.service.exception.GroupNotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class ExamServiceTest {

	private ExamAttendanceDTO examAttendanceDTO;

	private BhUserDTO bhUserDTO;

	private ExamDTO examDTO;

	private EmailDTO emailDTO;

	private DataFactory dataFactory;

	private List<ExamAttendanceDTO> examResultList;

	@Mock
	private ExamDAO examDAO;
	@Mock
	private BhGroupDAO groupDAO;
	@Mock
	private ExamAttendanceDAO examAttendanceDao;
	@Mock
	private BhUserDAO userDAO;
	@Mock
	private ExamFileDAO examFileDAO;
	@Mock
	private EmailEventSender mockedEmailEventSender;

	@InjectMocks
	private DefaultExamService service;

	@InjectMocks
	private DefaultEmailEventSender emailEventSender;

	private List<BhGroupDTO> allGroupDTOs;
	private List<BhUserDTO> allUserDTOs;
	private List<ExamDTO> allExamDTOs;
	private List<ExamAttendanceDTO> allExamAttendanceDTOs;

	private String[] lastNames = { "Cotton", "Craft", "Cannon", "Larsen", "Ruiz", "Levine", "Santiago", "Smith",
			"McGuire" };
	private String[] firstNames = { "Karen", "Leslie", "Eddie", "Mariah", "Nancy", "Wayne", "Chasity", "Terri",
			"Aaron" };

	@Before
	public void init() {

		MockitoAnnotations.initMocks(this);
		examResultList = new ArrayList<>();

		bhUserDTO = new BhUserDTO();
		bhUserDTO.setEmailAddress("alex.fabian0330@gmail.com");

		for (int i = 0; i < 10; i++) {
			examAttendanceDTO = new ExamAttendanceDTO();
			examDTO = new ExamDTO();
			examDTO.setExamDate(new Date());
			examDTO.setMaxScorePractice(new BigDecimal(120));
			examDTO.setMaxScoreTheory(new BigDecimal(120));

			examAttendanceDTO.setExam(examDTO);
			examAttendanceDTO.setUser(bhUserDTO);
			examAttendanceDTO.setId(Integer.toUnsignedLong(i));
			if (i == 4) {
				examAttendanceDTO.setScorePractice(new BigDecimal(50));
				examAttendanceDTO.setScoreTheory(new BigDecimal(60));
			} else {
				examAttendanceDTO.setScorePractice(new BigDecimal((int) (Math.random() * 120)));
				examAttendanceDTO.setScoreTheory(new BigDecimal((int) (Math.random() * 120)));
			}
			examResultList.add(examAttendanceDTO);
		}

		bhUserDTO.setExamResultList(examResultList);
		String emailBody = "Practice: " + examAttendanceDTO.getScorePractice() + "\nTheory: "
				+ examAttendanceDTO.getScoreTheory();
		emailDTO = new EmailDTO(bhUserDTO.getEmailAddress(), "Your test score", emailBody);

		// New Stuff

		// dataFactory = new DataFactory();

		TestDummyDTOs dummyDTOs = new TestDummyDTOs();

		dummyDTOs.initDTOs();

		allExamDTOs = dummyDTOs.getAllExamDTOs();
		allExamAttendanceDTOs = dummyDTOs.getAllExamAttendanceDTOs();
		allUserDTOs = new ArrayList<>();

		for (Optional<BhUserDTO> user : dummyDTOs.getAllUserDTOs()) {

			allUserDTOs.add(user.get());
		}
		
		allGroupDTOs = dummyDTOs.getAllGroupDTOs();

		dummyDTOs.setExamAttendanceDAO(examAttendanceDao);
		dummyDTOs.setExamDAO(examDAO);
		dummyDTOs.setGroupDAO(groupDAO);
		dummyDTOs.setUserDAO(userDAO);
		dummyDTOs.setExamFileDAO(examFileDAO);

		dummyDTOs.initDAOs();

		examAttendanceDao = dummyDTOs.getExamAttendanceDAO();
		examDAO = dummyDTOs.getExamDAO();
		userDAO = dummyDTOs.getUserDAO();
		groupDAO = dummyDTOs.getGroupDAO();
		examFileDAO = dummyDTOs.getExamFileDAO();

	}

	@Test
	public void testAddNewExamWorks() throws DatabaseException {

		Long examId = 1L;
		int examIndex = examId.intValue() - 1;
		Mockito.when(examDAO.getById(examIndex - 1L)).thenReturn(allExamDTOs.get(examIndex));
		Mockito.when(examDAO.getById(3L)).thenReturn(null);

		ArgumentCaptor<ExamDTO> addedExam = ArgumentCaptor.forClass(ExamDTO.class);
		service.addNewExam(examDTO);
		verify(examDAO, times(1)).insert(addedExam.capture());

	}

	@Test(expected = DatabaseException.class)
	public void testAddNewExamFailsFromDAO() throws DatabaseException {

		Mockito.when(examDAO.insert(examDTO)).thenThrow(RuntimeException.class);

		ArgumentCaptor<ExamDTO> addedExam = ArgumentCaptor.forClass(ExamDTO.class);
		service.addNewExam(examDTO);
		verify(examDAO, times(1)).insert(addedExam.capture());

	}

	@Test
	public void testCreateNewExamWorks() throws ParseException, DatabaseException {

		ArgumentCaptor<ExamDTO> addedExamCaptor = ArgumentCaptor.forClass(ExamDTO.class);
		service.createNewExam("2018-08-25-12-00-00", "Exam 4", new BigDecimal(200), new BigDecimal(200));
		verify(examDAO, times(1)).insert(addedExamCaptor.capture());

		ExamDTO addedExam = addedExamCaptor.getValue();
		assertTrue((new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").parse("2018-08-25-12-00-00"))
				.equals(addedExam.getExamDate()));
		assertTrue("Exam 4".equals(addedExam.getExamTitle()));

	}

	@Test(expected = ParseException.class)
	public void testCreateNewExamWrongDateFormat() throws ParseException, DatabaseException {

		ArgumentCaptor<ExamDTO> addedExamCaptor = ArgumentCaptor.forClass(ExamDTO.class);
		service.createNewExam("2018-08-25-12-0", "Exam 4", new BigDecimal(200), new BigDecimal(200));
		verify(examDAO, times(1)).insert(addedExamCaptor.capture());

	}

	@Test
	public void testExamDetailsProperlyForUser() {

		Map<Integer, List<String>> examDetailsList = service.getExamDetailsByUser(bhUserDTO);

		String practiceScore = examDetailsList.get(4).get(0);
		String practiceAverage = examDetailsList.get(4).get(1);
		String theoryScore = examDetailsList.get(4).get(2);
		String theoryAverage = examDetailsList.get(4).get(3);

		Assert.assertNotNull(examDetailsList);
		System.out.println(practiceAverage);
		Assert.assertEquals(practiceScore, "50");

		Assert.assertEquals(practiceAverage, " 41.67 %");
		Assert.assertEquals(theoryScore, "60");
		Assert.assertEquals(theoryAverage, " 50.00 %");
	}

	@Test
	public void testGenerationOfDummyValuesWork() {
		assertEquals(3, allExamDTOs.size());
		assertEquals(9, allUserDTOs.size());
		assertEquals(3, allGroupDTOs.size());
		assertEquals(18, allExamAttendanceDTOs.size());
		for (int i = 0; i < 3; i++) {
			assertEquals(allUserDTOs.subList(0, 3).get(i), allGroupDTOs.get(0).getBhUserList().get(i));
		}
		for (int i = 0; i < allExamDTOs.get(1).getExamResultList().size(); i++) {

			assertEquals(allExamDTOs.get(1).getExamResultList().get(i).getExam(), allExamDTOs.get(1));

			BhUserDTO user = allGroupDTOs.get(1).getBhUserList().get(0);
			assertEquals(user.getExamResultList().get(1).getUser(), user);
		}

	}

	@Test
	public void testGetAllAverageExamResultsWorks() throws BusinessObjectNotFoundException, DatabaseException {

		Mockito.when(groupDAO.getAll()).thenReturn(allGroupDTOs);

		Long groupId = 3L;
		int groupIndex = groupId.intValue() - 1;
		for (int j = 0; j < 2; j++) {
			for (int i = 0; i < 3; i++) {
				assertEquals(3, allGroupDTOs.get(groupIndex).getBhUserList().get(i).getExamResultList().size());

				allGroupDTOs.get(groupIndex).getBhUserList().get(i).getExamResultList().get(j)
						.setScorePractice(new BigDecimal(i * 10 + 100));

				allGroupDTOs.get(groupIndex).getBhUserList().get(i).getExamResultList().get(j)
						.setScoreTheory(new BigDecimal(j * 10 + 120));
			}
		}

		Map<String, Map<String, Double>> returnValue = service.getAllAverageExamResults();

	}

	@Test(expected = DatabaseException.class)
	public void testGetAllAverageExamResultsFailsFromDAO() throws BusinessObjectNotFoundException, DatabaseException {

		Mockito.when(groupDAO.getAll()).thenThrow(new RuntimeException());

		Map<String, Map<String, Double>> returnValue = service.getAllAverageExamResults();
	}

	@Test
	public void testGetAllExamAttendanceDTOWorks() throws DatabaseException {

		Mockito.when(examAttendanceDao.getAll()).thenReturn(allExamAttendanceDTOs);
		List<ExamAttendanceDTO> returnValues = service.getAllExamAttendanceDTO();

	}

	@Test(expected = DatabaseException.class)
	public void testGetAllExamAttendanceDTOFailsByDAO() throws DatabaseException {

		Mockito.when(examAttendanceDao.getAll()).thenThrow(RuntimeException.class);
		List<ExamAttendanceDTO> returnValues = service.getAllExamAttendanceDTO();

	}

	@Test(expected = DatabaseException.class)
	public void testGetAllExamAttendanceDTOFailsByNullReturned() throws DatabaseException {

		Mockito.when(examAttendanceDao.getAll()).thenReturn(null);
		List<ExamAttendanceDTO> returnValues = service.getAllExamAttendanceDTO();

	}

	@Test
	public void testGetAllExamDTOWorks() throws DatabaseException {

		Mockito.when(examDAO.getAll()).thenReturn(allExamDTOs);
		List<ExamDTO> returnValues = service.getAllExamDTO();

	}

	@Test(expected = DatabaseException.class)
	public void testGetAllExamDTOFailsFromDAO() throws DatabaseException {

		Mockito.when(examDAO.getAll()).thenThrow(RuntimeException.class);
		List<ExamDTO> returnValues = service.getAllExamDTO();

	}

	@Test(expected = DatabaseException.class)
	public void testGetAllExamDTOFailsFromNullReturned() throws DatabaseException {

		Mockito.when(examDAO.getAll()).thenReturn(null);
		List<ExamDTO> returnValues = service.getAllExamDTO();

	}

	@Test
	public void testGetAverageExamResultsByGroupWorks() throws BusinessObjectNotFoundException, DatabaseException {

		Long groupId = 3L;
		int groupIndex = groupId.intValue() - 1;
		String groupName = allGroupDTOs.get(groupIndex).getName();
		Mockito.when(groupDAO.getByName(groupName)).thenReturn(allGroupDTOs.get(groupIndex));

		for (int j = 0; j < 2; j++) {
			for (int i = 0; i < 3; i++) {
				assertEquals(3, allGroupDTOs.get(groupIndex).getBhUserList().get(i).getExamResultList().size());

				allGroupDTOs.get(groupIndex).getBhUserList().get(i).getExamResultList().get(j)
						.setScorePractice(new BigDecimal(i * 10 + 100));

				allGroupDTOs.get(groupIndex).getBhUserList().get(i).getExamResultList().get(j)
						.setScoreTheory(new BigDecimal(j * 10 + 120));
			}
		}

		HashMap<String, Double> returnValues = (HashMap<String, Double>) service
				.getAverageExamResultsByGroup(groupName);

		assertTrue((double) returnValues.get(allExamDTOs.get(0).getExamTitle()) == 57.5);

		assertTrue((double) returnValues.get(allExamDTOs.get(2).getExamTitle()) == 0);

	}

	@Test // (expected = DatabaseException.class)
	public void testGetAverageExamResultsByGroupFailsFromDAO()
			throws BusinessObjectNotFoundException, DatabaseException {

		Long groupId = 3L;
		int groupIndex = groupId.intValue() - 1;
		String groupName = allGroupDTOs.get(groupIndex).getName();
		Mockito.when(groupDAO.getByName(groupName)).thenThrow(RuntimeException.class);

		for (int j = 0; j < 2; j++) {
			for (int i = 0; i < 3; i++) {
				assertEquals(3, allGroupDTOs.get(groupIndex).getBhUserList().get(i).getExamResultList().size());

				allGroupDTOs.get(groupIndex).getBhUserList().get(i).getExamResultList().get(j)
						.setScorePractice(new BigDecimal(i * 10 + 100));

				allGroupDTOs.get(groupIndex).getBhUserList().get(i).getExamResultList().get(j)
						.setScoreTheory(new BigDecimal(j * 10 + 120));
			}
		}

		try {

			HashMap<String, Double> returnValues = (HashMap<String, Double>) service
					.getAverageExamResultsByGroup(groupName);

			assertTrue((double) returnValues.get(allExamDTOs.get(0).getExamTitle()) == 57.5);

			assertTrue((double) returnValues.get(allExamDTOs.get(2).getExamTitle()) == 0);

		} catch (Exception e) {

			assertTrue(e.getClass() == DatabaseException.class);
			assertTrue(("Could not get group " + groupName + " from database!").equals(e.getMessage()));
		}

	}

	@Test
	public void testGetExamAttendanceDTOWorks() throws BusinessObjectNotFoundException, DatabaseException {

		ExamAttendanceDTO returnValue = service.getExamAttendanceDTO(1L);
		System.out.println(returnValue.getExam().getExamTitle());
	}

	@Test(expected = DatabaseException.class)
	public void testGetExamAttendanceDTOFailsFromDAO() throws BusinessObjectNotFoundException, DatabaseException {

		Mockito.when(examAttendanceDao.getById(1L)).thenThrow(RuntimeException.class);

		ExamAttendanceDTO returnValue = service.getExamAttendanceDTO(1L);
	}

	@Test
	public void testGetExamDetailsByExamWorks() throws BusinessObjectNotFoundException {

		Long examId = 1L;
		int examIndex = examId.intValue() - 1;
		Mockito.when(examDAO.getById(examId)).thenReturn(allExamDTOs.get(examIndex));

		for (ExamAttendanceDTO result : allUserDTOs.get(0).getExamResultList()) {
			result.setScorePractice(new BigDecimal(140));
			result.setScoreTheory(new BigDecimal(120));
		}

		Map<Integer, List<String>> details = service.getExamDetailsByExam(examId);

		assertNotNull(details);
		assertNotEquals(0, details.size());

		for (int key : details.keySet()) {
			System.out.print(key + " - ");
			System.out.println(details.get(key));
		}
		assertEquals(" 70.00 %", details.get(0).get(5));
	}

	@Test(expected = ExamNotFoundException.class)
	public void testGetExamDetailsByExamFailsFromDAO() throws BusinessObjectNotFoundException {

		Mockito.when(examDAO.getById(1L)).thenThrow(RuntimeException.class);

		Map<Integer, List<String>> returnValue = service.getExamDetailsByExam(1L);

	}

	@Test
	public void testGetExamDTOWorks() throws BusinessObjectNotFoundException, DatabaseException {

		ExamDTO returnValue = service.getExamDTO(2L);

	}

	@Test(expected = DatabaseException.class)
	public void testGetExamDTOFails() throws BusinessObjectNotFoundException, DatabaseException {

		Mockito.when(examDAO.getById(1L)).thenThrow(RuntimeException.class);

		ExamDTO returnValue = service.getExamDTO(1L);
	}

	@Test
	public void testLinkExamToGroupWorks() throws BusinessObjectNotFoundException, DatabaseException {

		int examIndex = 2;
		int groupIndex = 1;

		Mockito.when(examDAO.getById(examIndex - 1L)).thenReturn(allExamDTOs.get(examIndex));
		Mockito.when(groupDAO.getById(groupIndex - 1L)).thenReturn(allGroupDTOs.get(groupIndex));

		int index = 0;
		for (BhUserDTO userDTO : allGroupDTOs.get(1).getBhUserList()) {
			assertNotEquals(userDTO.getExamResultList().get(userDTO.getExamResultList().size() - 1),
					allExamDTOs.get(examIndex));
			for (ExamAttendanceDTO examAttendanceDTO : userDTO.getExamResultList()) {
				index++;
				assertNotEquals(allExamDTOs.get(examIndex), examAttendanceDTO.getExam());
			}
		}

		ArgumentCaptor<BhUserDTO> insertedUser = ArgumentCaptor.forClass(BhUserDTO.class);
		service.linkExamToGroup(examIndex - 1L, groupIndex - 1L);
		verify(userDAO, atLeastOnce()).update(insertedUser.capture());

		assertTrue(allUserDTOs.contains(insertedUser.getAllValues().get(0)));
		for (BhUserDTO user : insertedUser.getAllValues()) {
			assertTrue(allGroupDTOs.get(groupIndex).getBhUserList().contains(user));
			assertEquals(user.getExamResultList().get(user.getExamResultList().size() - 1).getExam(),
					allExamDTOs.get(examIndex));
		}
	}

	@Test(expected = ExamNotFoundException.class)
	public void testLinkExamToGroupFailsFromExamDao() throws BusinessObjectNotFoundException, DatabaseException {

		Mockito.when(examDAO.getById(4L)).thenThrow(RuntimeException.class);

		service.linkExamToGroup(4L, 2L);

	}

	@Test(expected = GroupNotFoundException.class)
	public void testLinkExamToGroupFailsFromGroupDao() throws BusinessObjectNotFoundException, DatabaseException {

		Mockito.when(groupDAO.getById(4L)).thenThrow(RuntimeException.class);

		service.linkExamToGroup(2L, 4L);

	}

	@Test
	public void testUpdateExamWorks() throws DatabaseException {

		ArgumentCaptor<ExamDTO> updatedExamDTOCaptor = ArgumentCaptor.forClass(ExamDTO.class);
		service.updateExam(allExamDTOs.get(2));
		verify(examDAO, times(1)).update(updatedExamDTOCaptor.capture());
		assertEquals(allExamDTOs.get(2).getExamTitle(), updatedExamDTOCaptor.getValue().getExamTitle());

	}

	@Test(expected = DatabaseException.class)
	public void testUpdateExamFailsFromDAO() throws DatabaseException {

		doThrow(RuntimeException.class).when(examDAO).update(allExamDTOs.get(2));

		ArgumentCaptor<ExamDTO> updatedExamDTOCaptor = ArgumentCaptor.forClass(ExamDTO.class);
		service.updateExam(allExamDTOs.get(2));
		verify(examDAO, times(1)).update(updatedExamDTOCaptor.capture());
		assertEquals(allExamDTOs.get(2).getExamTitle(), updatedExamDTOCaptor.getValue().getExamTitle());

	}

	@Test
	public void testUpdateExamAttendanceWorks() throws DatabaseException {

		service.updateExamAttendance(allExamAttendanceDTOs.get(0));

	}

	@Test(expected = DatabaseException.class)
	public void testUpdateExamAttendanceFailsFromDAO() throws DatabaseException {

		doThrow(RuntimeException.class).when(examAttendanceDao).update(allExamAttendanceDTOs.get(0));
		service.updateExamAttendance(allExamAttendanceDTOs.get(0));

	}

	@Test
	public void testUpdateExamAttendanceWithNotNullScore() throws DatabaseException {

		allExamAttendanceDTOs.get(0).setScorePractice(new BigDecimal(100));
		allExamAttendanceDTOs.get(0).setScoreTheory(new BigDecimal(120));

		service.updateExamAttendance(allExamAttendanceDTOs.get(0));

	}

	@Test
	public void testUpdateExamAttendanceWithNullScore() throws DatabaseException {

		allExamAttendanceDTOs.get(0).setScorePractice(null);
		allExamAttendanceDTOs.get(0).setScoreTheory(null);

		service.updateExamAttendance(allExamAttendanceDTOs.get(0));

	}
}
