package hu.braininghub.bh06.ejb.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import hu.braininghub.bh06.ess.dao.BhGroupDAO;
import hu.braininghub.bh06.ess.dao.BhUserDAO;
import hu.braininghub.bh06.ess.dao.ExamAttendanceDAO;
import hu.braininghub.bh06.ess.dao.ExamDAO;
import hu.braininghub.bh06.ess.dao.ExamFileDAO;
import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;
import hu.braininghub.bh06.ess.dto.ExamDTO;
import hu.braininghub.bh06.ess.service.DefaultUserService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.UserNotFoundException;
import hu.braininghub.bh06.ess.service.exception.UsernameInUseException;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	
	private Optional<BhUserDTO> testDTO = Optional.ofNullable(new BhUserDTO());

	@Mock
	private ExamDAO examDAO;
	@Mock
	private BhGroupDAO groupDAO;
	@Mock
	private ExamAttendanceDAO examAttendanceDao;
	@Mock
	private BhUserDAO userDAO;
	@Mock
	private ExamFileDAO examFileDAO;

	@InjectMocks
	DefaultUserService service;

	private List<BhGroupDTO> allGroupDTOs;
	private List<Optional<BhUserDTO>> allUserDTOs;
	private List<ExamDTO> allExamDTOs;
	private List<ExamAttendanceDTO> allExamAttendanceDTOs;
	
	private TestDummyDTOs dummyDTOs;
	
	@Before
	public void init() {

		BhUserDTO test = testDTO.get();
		
		test.setActive(1);
		test.setBhPass("pass");
		test.setEmailAddress("email@mail.com");
		test.setExamFileList(null);
		test.setExamResultList(null);
		test.setFirstName("Endre");
		test.setGroup(null);
		test.setLastName("Tesztes");
		test.setRight(null);
		test.setSessionList(null);
		test.setUserName("enci");
		test.setId(1L);

		dummyDTOs = new TestDummyDTOs();

		dummyDTOs.initDTOs();

		allExamDTOs = dummyDTOs.getAllExamDTOs();
		allExamAttendanceDTOs = dummyDTOs.getAllExamAttendanceDTOs();
		allUserDTOs = dummyDTOs.getAllUserDTOs();
		allGroupDTOs = dummyDTOs.getAllGroupDTOs();

		dummyDTOs.setExamAttendanceDAO(examAttendanceDao);
		dummyDTOs.setExamDAO(examDAO);
		dummyDTOs.setGroupDAO(groupDAO);
		dummyDTOs.setUserDAO(userDAO);
		dummyDTOs.setExamFileDAO(examFileDAO);

		dummyDTOs.initDAOs();

		examAttendanceDao = dummyDTOs.getExamAttendanceDAO();
		examDAO = dummyDTOs.getExamDAO();
		userDAO = dummyDTOs.getUserDAO();
		groupDAO = dummyDTOs.getGroupDAO();
		examFileDAO = dummyDTOs.getExamFileDAO();
	}

	@Test
	public final void testGetUserById() throws UserNotFoundException, DatabaseException {
		String actualName = service.getBhUserDTOById(2L).getUserName();
		String expectedName = "Leslie.Craft";
		
		assertEquals(expectedName, actualName);
		assertEquals(service.getBhUserDTOById(3L).getId().toString(), "3");
	}
	
	@Test(expected = DatabaseException.class)
	public final void testGetUserByIdFails() throws UserNotFoundException, DatabaseException {
		
		Mockito.when(userDAO.getById(2L)).thenThrow(RuntimeException.class);
		service.getBhUserDTOById(2L);
		
//		String actualName = service.getBhUserDTOById(2L).getUserName();
//		String expectedName = "Leslie.Craft";
//		
//		assertEquals(expectedName, actualName);
//		assertEquals(service.getBhUserDTOById(3L).getId().toString(), "3");
	}
	
	@Test
	public final void testGetUserByName() throws UserNotFoundException, DatabaseException {
		
		String actualName = service.getBhUserDTOByUsername(allUserDTOs.get(0).get().getUserName()).getUserName();

		String expectedName = "Karen.Cotton";

		assertEquals(expectedName, actualName);
	}
	
	@Test(expected = RuntimeException.class)
	public final void testGetUserByNameFails() throws UserNotFoundException, DatabaseException {
		
		Mockito.when(userDAO.getByUserName("Laci")).thenThrow(RuntimeException.class);
		service.getBhUserDTOByUsername("Laci");
		
	}

	@Test
	public final void testUpdatePassword() throws DatabaseException, BusinessObjectNotFoundException {
		String username = service.getBhUserDTOById(2L).getUserName();
		String oldPassword = "oldpassword";
		testDTO.get().setBhPass(oldPassword);
		String newPassword = "1337";
		Mockito.when(userDAO.getByUserName(username)).thenReturn(testDTO);
		
		doAnswer(new Answer<Void>() {

			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				testDTO.get().setBhPass(newPassword);
				return null;	
			}
			
		}).when(userDAO).update(testDTO.get());
		
		service.changeUserPassword(username, oldPassword);
		assertEquals(newPassword, testDTO.get().getBhPass());
	}
	
	@Test(expected = DatabaseException.class)
	public final void testUpdatePasswordFails() throws DatabaseException, BusinessObjectNotFoundException {
		
		Mockito.when(userDAO.getByUserName("laci")).thenThrow(RuntimeException.class);
		service.changeUserPassword("laci", "password");
	}
	
	@Test(expected = DatabaseException.class)
	public final void testUpdatePasswordFailsInUpdate() throws DatabaseException, BusinessObjectNotFoundException {
		
		doThrow(RuntimeException.class).when(userDAO).update(dummyDTOs.getAllUserDTOs().get(1).get());
		service.changeUserPassword(dummyDTOs.getAllUserDTOs().get(1).get().getUserName(), "password");
	}
	
	@Test
	public final void testNewUserCreation() throws UsernameInUseException, DatabaseException {
		Mockito.when(userDAO.getById(testDTO.get().getId())).thenReturn(null);
		List<BhUserDTO> users = new ArrayList<>();
		testDTO.get().setUserName("Cartman");
		
		doAnswer(new Answer<Void>() {

			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				users.add(testDTO.get());
				return null;
			}
			
		}).when(userDAO).insert(testDTO.get());
		
		service.createNewUser(testDTO.get());
		assertEquals(1, users.size());
		assertEquals("Cartman", users.get(0).getUserName());
	}
	
	@Test
	public final void testGetAllStudents() {
		List<BhUserDTO> students = service.getAllStudents();
		
		assertEquals("Mariah", students.get(3).getFirstName());
		assertEquals(9, students.size());
	}
	
	
}
