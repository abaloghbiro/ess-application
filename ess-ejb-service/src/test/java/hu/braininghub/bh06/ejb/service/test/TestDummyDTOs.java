package hu.braininghub.bh06.ejb.service.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import org.mockito.Mockito;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;
import hu.braininghub.bh06.ess.dao.BhGroupDAO;
import hu.braininghub.bh06.ess.dao.BhUserDAO;
import hu.braininghub.bh06.ess.dao.ExamAttendanceDAO;
import hu.braininghub.bh06.ess.dao.ExamDAO;
import hu.braininghub.bh06.ess.dao.ExamFileDAO;
import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;
import hu.braininghub.bh06.ess.dto.ExamDTO;
import hu.braininghub.bh06.ess.dto.ExamFileDTO;
import hu.braininghub.bh06.ess.dto.RightDTO;

public class TestDummyDTOs {

	private String[] lastNames = { "Cotton", "Craft", "Cannon", "Larsen", "Ruiz", "Levine", "Santiago", "Smith",
			"McGuire" };
	private String[] firstNames = { "Karen", "Leslie", "Eddie", "Mariah", "Nancy", "Wayne", "Chasity", "Terri",
			"Aaron" };

	private List<BhGroupDTO> allGroupDTOs;
	private List<Optional<BhUserDTO>> allUserDTOs;
	private List<ExamDTO> allExamDTOs;
	private List<ExamAttendanceDTO> allExamAttendanceDTOs;
	private List<ExamFileDTO> allExamFileDTOs;

	private ExamDAO examDAO;
	private BhGroupDAO groupDAO;
	private ExamAttendanceDAO examAttendanceDAO;
	private BhUserDAO userDAO;
	private ExamFileDAO examFileDAO;

	public ExamFileDAO getExamFileDAO() {
		return examFileDAO;
	}

	public void setExamFileDAO(ExamFileDAO examFileDAO) {
		this.examFileDAO = examFileDAO;
	}

	public String[] getLastNames() {
		return lastNames;
	}

	public String[] getFirstNames() {
		return firstNames;
	}

	public List<BhGroupDTO> getAllGroupDTOs() {
		return allGroupDTOs;
	}

	public List<Optional<BhUserDTO>> getAllUserDTOs() {
		return allUserDTOs;
	}

	public List<ExamDTO> getAllExamDTOs() {
		return allExamDTOs;
	}

	public List<ExamAttendanceDTO> getAllExamAttendanceDTOs() {
		return allExamAttendanceDTOs;
	}

	public List<ExamFileDTO> getAllExamFileDTOs() {
		return allExamFileDTOs;
	}

	public void initDTOs() {

		allGroupDTOs = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			BhGroupDTO currentGroupDTO = new BhGroupDTO();
			currentGroupDTO.setBhUserList(new ArrayList<BhUserDTO>());
			currentGroupDTO.setName("BH" + (i + 1));
			currentGroupDTO.setId(1L + i);
			allGroupDTOs.add(currentGroupDTO);
		}

		allUserDTOs = new ArrayList<>();
		RightDTO right = new RightDTO();
		right.setAccessLevel(AccessLevels.STUDENT);
		for (int i = 0; i < 9; i++) {
			Optional<BhUserDTO> currentUserDTOOp = Optional.ofNullable(new BhUserDTO());
			BhUserDTO currentUserDTO = currentUserDTOOp.get();
			currentUserDTO.setId(i + 1L);
			currentUserDTO.setFirstName(firstNames[i]);
			currentUserDTO.setLastName(lastNames[i]);
			currentUserDTO.setUserName(currentUserDTO.getFirstName() + "." + currentUserDTO.getLastName());
			currentUserDTO.setActive(1);
			currentUserDTO.setEmailAddress(currentUserDTO.getUserName() + "@ess.bh.hu");
			currentUserDTO.setRight(right);
			currentUserDTO.setExamFileList(new ArrayList<ExamFileDTO>());
			currentUserDTO.setExamResultList(new ArrayList<ExamAttendanceDTO>());
			allGroupDTOs.get(i / 3).getBhUserList().add(currentUserDTO);
			currentUserDTO.setGroup(allGroupDTOs.get(i / 3));
			allUserDTOs.add(currentUserDTOOp);
		}

		allExamDTOs = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			ExamDTO currentExamDTO = new ExamDTO();
			currentExamDTO.setExamDate(new GregorianCalendar(2018, 8, 8 + i, 9, 00, 00).getTime());
			currentExamDTO.setExamState(1);
			currentExamDTO.setExamTitle("Exam " + (i + 1));
			currentExamDTO.setExamFileList(new ArrayList<ExamFileDTO>());
			currentExamDTO.setExamResultList(new ArrayList<ExamAttendanceDTO>());
			currentExamDTO.setId(i + 1L);
			currentExamDTO.setMaxScorePractice(new BigDecimal(200));
			currentExamDTO.setMaxScoreTheory(new BigDecimal(200));
			allExamDTOs.add(currentExamDTO);
		}

		allExamFileDTOs = new ArrayList<>();

		allExamAttendanceDTOs = new ArrayList<>();
		for (int i = 0; i < 3; i++) { // i -> group
			for (int j = 0; j <= i; j++) { // j -> exam

				for (int k = 0; k < allGroupDTOs.get(i).getBhUserList().size(); k++) { // -> user
					ExamAttendanceDTO currentExamAttendanceDTO = new ExamAttendanceDTO();
					currentExamAttendanceDTO.setId(i * 3 + j + 1L);
					currentExamAttendanceDTO.setExam(allExamDTOs.get(j));
					allExamDTOs.get(j).getExamResultList().add(currentExamAttendanceDTO);
					BhUserDTO currentUser = allGroupDTOs.get(i).getBhUserList().get(k);
					currentUser.getExamResultList().add(currentExamAttendanceDTO);
					currentExamAttendanceDTO.setUser(currentUser);
					allExamAttendanceDTOs.add(currentExamAttendanceDTO);

					ExamFileDTO currentExamFileDTO = new ExamFileDTO();
					currentExamFileDTO.setId(i * 3 + j + 1L);
					currentExamFileDTO.setExam(allExamDTOs.get(j));
					allExamDTOs.get(j).getExamFileList().add(currentExamFileDTO);
					currentUser.getExamFileList().add(currentExamFileDTO);
					currentExamFileDTO.setUser(currentUser);
					allExamFileDTOs.add(currentExamFileDTO);

				}
			}

		}

	}

	public void initDAOs() {

		for (BhGroupDTO groupDTO : allGroupDTOs) {
			Mockito.when(groupDAO.getById(groupDTO.getId())).thenReturn(groupDTO);
			Mockito.when(groupDAO.getByName(groupDTO.getName())).thenReturn(groupDTO);
			Mockito.when(groupDAO.getAll()).thenReturn(allGroupDTOs);
		}

		List<BhUserDTO> allUsers = new ArrayList<>();

		for (Optional<BhUserDTO> userDTO : allUserDTOs) {
			Mockito.when(userDAO.getById(userDTO.get().getId())).thenReturn(userDTO.get());
			Mockito.when(userDAO.getByUserName(userDTO.get().getUserName())).thenReturn(userDTO);
			allUsers.add(userDTO.get());

		}

		Mockito.when(userDAO.getAll()).thenReturn(allUsers);

		for (ExamDTO examDTO : allExamDTOs) {
			Mockito.when(examDAO.getById(examDTO.getId())).thenReturn(examDTO);
			Mockito.when(examDAO.getAll()).thenReturn(allExamDTOs);
		}

		for (ExamAttendanceDTO examAttendanceDTO : allExamAttendanceDTOs) {
			Mockito.when(examAttendanceDAO.getById(examAttendanceDTO.getId())).thenReturn(examAttendanceDTO);
			Mockito.when(examAttendanceDAO.getAll()).thenReturn(allExamAttendanceDTOs);
		}

		for (ExamFileDTO examFileDTO : allExamFileDTOs) {
			Mockito.when(examFileDAO.getById(examFileDTO.getId())).thenReturn(examFileDTO);
			Mockito.when(examFileDAO.getAll()).thenReturn(allExamFileDTOs);
		}

	}

	public ExamDAO getExamDAO() {
		return examDAO;
	}

	public void setExamDAO(ExamDAO examDAO) {
		this.examDAO = examDAO;
	}

	public BhGroupDAO getGroupDAO() {
		return groupDAO;
	}

	public void setGroupDAO(BhGroupDAO groupDAO) {
		this.groupDAO = groupDAO;
	}

	public ExamAttendanceDAO getExamAttendanceDAO() {
		return examAttendanceDAO;
	}

	public void setExamAttendanceDAO(ExamAttendanceDAO examAttendanceDAO) {
		this.examAttendanceDAO = examAttendanceDAO;
	}

	public BhUserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(BhUserDAO userDAO) {
		this.userDAO = userDAO;
	}

}
