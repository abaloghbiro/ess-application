package hu.braininghub.bh06.ejb.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import hu.braininghub.bh06.ess.dao.BhGroupDAO;
import hu.braininghub.bh06.ess.dao.BhUserDAO;
import hu.braininghub.bh06.ess.dao.ExamAttendanceDAO;
import hu.braininghub.bh06.ess.dao.ExamDAO;
import hu.braininghub.bh06.ess.dao.ExamFileDAO;
import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;
import hu.braininghub.bh06.ess.dto.ExamFileDTO;
import hu.braininghub.bh06.ess.service.DefaultExamFileService;
import hu.braininghub.bh06.ess.service.DefaultExamService;
import hu.braininghub.bh06.ess.service.ExamFileService;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.ExamFileNotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class ExamFileServiceTest {

	@Mock private BhGroupDAO groupDAO;
	@Mock private BhUserDAO userDAO;
	@Mock private ExamDAO examDAO;
	@Mock private ExamAttendanceDAO examAttendanceDAO;
	@Mock private ExamFileDAO examFileDao;

	private TestDummyDTOs dummyDTOs;
	
	@InjectMocks private DefaultExamFileService examFileService;
	@InjectMocks private DefaultExamService examService;
	
	@Before
	public void init() throws Exception {
		
		dummyDTOs = new TestDummyDTOs();
		
		dummyDTOs.initDTOs();
		
		dummyDTOs.setExamAttendanceDAO(examAttendanceDAO);
		dummyDTOs.setExamDAO(examDAO);
		dummyDTOs.setGroupDAO(groupDAO);
		dummyDTOs.setUserDAO(userDAO);
		dummyDTOs.setExamFileDAO(examFileDao);
		
		dummyDTOs.initDAOs();
		
		examAttendanceDAO = dummyDTOs.getExamAttendanceDAO();
		examDAO = dummyDTOs.getExamDAO();
		groupDAO = dummyDTOs.getGroupDAO();
		userDAO = dummyDTOs.getUserDAO();
		examFileDao = dummyDTOs.getExamFileDAO();
		
	}
	
	@Test
	public final void testNewMocking() throws BusinessObjectNotFoundException, DatabaseException {
		
		System.out.println(dummyDTOs.getAllGroupDTOs().get(2).getBhUserList().get(0).getExamResultList().size());
		
		ExamAttendanceDTO returnValue = examService.getExamAttendanceDTO(1L);
		assertNotNull(returnValue);
		System.out.println(returnValue.getExam().getExamTitle());
		
	}

	@Test
	public final void testAddNewExamFileWorks() throws DatabaseException {

		ExamFileDTO examFileDTO = new ExamFileDTO();
		examFileDTO.setExam(dummyDTOs.getAllExamDTOs().get(0));
		examFileDTO.setPath("/bla/bla25");
		examFileDTO.setUploadTime(new Date());
		examFileDTO.setUser(dummyDTOs.getAllUserDTOs().get(0).get());
		
		ArgumentCaptor<ExamFileDTO> addedExamFileCaptor = ArgumentCaptor.forClass(ExamFileDTO.class);
		examFileService.addNewExamFile(examFileDTO);
		verify(examFileDao, times(1)).insert(addedExamFileCaptor.capture());
		assertTrue("/bla/bla25".equals(addedExamFileCaptor.getValue().getPath()));
		
		
	}
	
	@Test(expected = DatabaseException.class)
	public final void testAddNewExamFileFailsByDAO() throws DatabaseException {
		

		
		ExamFileDTO examFileDTO = new ExamFileDTO();
		examFileDTO.setExam(dummyDTOs.getAllExamDTOs().get(0));
		examFileDTO.setPath("/bla/bla25");
		examFileDTO.setUploadTime(new Date());
		examFileDTO.setUser(dummyDTOs.getAllUserDTOs().get(0).get());
		
		doThrow(RuntimeException.class).when(examFileDao).insert(examFileDTO);
		examFileService.addNewExamFile(examFileDTO);
		
	}
	

	@Test
	public final void testGetAllExamFileDTOWorks() throws DatabaseException {
		
		examFileService.getAllExamFileDTO();
		
	}
	
	@Test(expected = DatabaseException.class)
	public final void testGetAllExamFileDTOFailsFromDAO() throws DatabaseException {
		
		Mockito.when(examFileDao.getAll()).thenThrow(RuntimeException.class);
		
		examFileService.getAllExamFileDTO();
		
	}
	
	@Test
	public final void testGetExamFileDTOWorks() throws BusinessObjectNotFoundException, DatabaseException {
		
		ExamFileDTO returnValue = examFileService.getExamFileDTO(5L);
		
	}
	

	@Test(expected = DatabaseException.class)
	public final void testGetExamFileDTOFailsFromDAO() throws BusinessObjectNotFoundException, DatabaseException {
		
		Mockito.when(examFileDao.getById(5L)).thenThrow(RuntimeException.class);
		
		ExamFileDTO returnValue = examFileService.getExamFileDTO(5L);
		
	}
	
	@Test
	public final void testUpdateExamFileWorks() throws DatabaseException {
		
		ArgumentCaptor<ExamFileDTO> examFileDTOCaptor = ArgumentCaptor.forClass(ExamFileDTO.class);
		examFileService.updateExamFile(dummyDTOs.getAllExamFileDTOs().get(5));
		verify(examFileDao, times(1)).update(examFileDTOCaptor.capture());

	}
	
	
	@Test(expected = DatabaseException.class)
	public final void testUpdateExamFileFailsFromDAO() throws DatabaseException {
		
		doThrow(RuntimeException.class).when(examFileDao).update(dummyDTOs.getAllExamFileDTOs().get(5));
		
		
		ArgumentCaptor<ExamFileDTO> examFileDTOCaptor = ArgumentCaptor.forClass(ExamFileDTO.class);
		examFileService.updateExamFile(dummyDTOs.getAllExamFileDTOs().get(5));
		verify(examFileDao, times(1)).update(examFileDTOCaptor.capture());

	}

}
