package hu.braininghub.bh06.ess.dao;

import java.util.Optional;

import hu.braininghub.bh06.ess.dto.BhUserDTO;

public interface BhUserDAO extends BaseDAO<BhUserDTO>{

	Optional<BhUserDTO> getByUserName(String userName);
  
}