package hu.braininghub.bh06.ess.dao;

import hu.braininghub.bh06.ess.dto.BhGroupDTO;


public interface BhGroupDAO extends BaseDAO<BhGroupDTO>{

	BhGroupDTO getByName(String name);

}
