package hu.braininghub.bh06.ess.dao;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;
import hu.braininghub.bh06.ess.dto.RightDTO;

public interface RightDAO extends BaseDAO<RightDTO>{

	RightDTO getByAccessLevels(AccessLevels level);
}

