package hu.braininghub.bh06.ess.dao;

import java.util.List;

import hu.braininghub.bh06.ess.dto.BusinessObjectDTO;

public interface BaseDAO<DTO extends BusinessObjectDTO> {
	
	void update(DTO object);
	
	Long insert(DTO object);
	
	void delete(DTO object);
	
	List<DTO> getAll();
	
	DTO getById(Long id);
}
