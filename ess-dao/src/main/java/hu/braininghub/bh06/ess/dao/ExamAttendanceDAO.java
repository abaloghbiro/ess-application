package hu.braininghub.bh06.ess.dao;

import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;

public interface ExamAttendanceDAO extends BaseDAO<ExamAttendanceDTO>{
	
}
