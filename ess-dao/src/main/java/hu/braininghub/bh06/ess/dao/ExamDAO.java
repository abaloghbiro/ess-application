package hu.braininghub.bh06.ess.dao;

import hu.braininghub.bh06.ess.dto.ExamDTO;

public interface ExamDAO extends BaseDAO<ExamDTO>{

}
