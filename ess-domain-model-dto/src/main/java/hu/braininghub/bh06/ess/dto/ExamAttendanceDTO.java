package hu.braininghub.bh06.ess.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExamAttendanceDTO extends BusinessObjectDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal scoreTheory;
	private BigDecimal scorePractice;
	private BhUserDTO user;
	private ExamDTO exam;

	public ExamAttendanceDTO() {
		super();
	}

	public BigDecimal getScoreTheory() {
		return scoreTheory;
	}

	public void setScoreTheory(BigDecimal scoreTheory) {
		this.scoreTheory = scoreTheory;
	}

	public BigDecimal getScorePractice() {
		return scorePractice;
	}

	public void setScorePractice(BigDecimal scorePractice) {
		this.scorePractice = scorePractice;
	}

	public BhUserDTO getUser() {
		return user;
	}

	public void setUser(BhUserDTO user) {
		this.user = user;
	}

	public ExamDTO getExam() {
		return exam;
	}

	public void setExam(ExamDTO exam) {
		this.exam = exam;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((exam == null) ? 0 : exam.hashCode());
		result = prime * result + ((scorePractice == null) ? 0 : scorePractice.hashCode());
		result = prime * result + ((scoreTheory == null) ? 0 : scoreTheory.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamAttendanceDTO other = (ExamAttendanceDTO) obj;
		if (exam == null) {
			if (other.exam != null)
				return false;
		} else if (!exam.equals(other.exam))
			return false;
		if (scorePractice == null) {
			if (other.scorePractice != null)
				return false;
		} else if (!scorePractice.equals(other.scorePractice))
			return false;
		if (scoreTheory == null) {
			if (other.scoreTheory != null)
				return false;
		} else if (!scoreTheory.equals(other.scoreTheory))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

}
