package hu.braininghub.bh06.ess.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;

public class RightDTO extends BusinessObjectDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private AccessLevels accessLevel;
	private List<BhUserDTO> bhUserList = new ArrayList<>();

	public RightDTO() {
		super();
	}

	public AccessLevels getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(AccessLevels accessLevel) {
		this.accessLevel = accessLevel;
	}

	public List<BhUserDTO> getBhUserList() {
		return bhUserList;
	}

	public void setBhUserList(List<BhUserDTO> bhUserList) {
		this.bhUserList = bhUserList;
	}

	@Override
	public String toString() {
		return "RightDTO [accessLevel=" + accessLevel + ", bhUserList=" + bhUserList + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accessLevel == null) ? 0 : accessLevel.hashCode());
		result = prime * result + ((bhUserList == null) ? 0 : bhUserList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RightDTO other = (RightDTO) obj;
		if (accessLevel != other.accessLevel)
			return false;
		if (bhUserList == null) {
			if (other.bhUserList != null)
				return false;
		} else if (!bhUserList.equals(other.bhUserList))
			return false;
		return true;
	}

}
