package hu.braininghub.bh06.ess.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExamDTO extends BusinessObjectDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date examDate;
	private String examTitle;
	private Integer examState;
	private BigDecimal maxScoreTheory;
	private BigDecimal maxScorePractice;
	private List<ExamAttendanceDTO> examResultList = new ArrayList<>();
	private List<ExamFileDTO> examFileList = new ArrayList<>();

	public ExamDTO() {
		super();
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public String getExamTitle() {
		return examTitle;
	}

	public void setExamTitle(String examTitle) {
		this.examTitle = examTitle;
	}

	public Integer getExamState() {
		return examState;
	}

	public void setExamState(Integer examState) {
		this.examState = examState;
	}

	public BigDecimal getMaxScoreTheory() {
		return maxScoreTheory;
	}

	public void setMaxScoreTheory(BigDecimal maxScoreTheory) {
		this.maxScoreTheory = maxScoreTheory;
	}

	public BigDecimal getMaxScorePractice() {
		return maxScorePractice;
	}

	public void setMaxScorePractice(BigDecimal maxScorePractice) {
		this.maxScorePractice = maxScorePractice;
	}

	public List<ExamAttendanceDTO> getExamResultList() {
		return examResultList;
	}

	public void setExamResultList(List<ExamAttendanceDTO> examResultList) {
		this.examResultList = examResultList;
	}

	public List<ExamFileDTO> getExamFileList() {
		return examFileList;
	}

	public void setExamFileList(List<ExamFileDTO> examFileList) {
		this.examFileList = examFileList;
	}

	public ExamDTO(Date examDate, String examTitle, Integer examState, BigDecimal maxScoreTheory,
			BigDecimal maxScorePractice, List<ExamAttendanceDTO> examResultList, List<ExamFileDTO> examFileList) {
		super();
		this.examDate = examDate;
		this.examTitle = examTitle;
		this.examState = examState;
		this.maxScoreTheory = maxScoreTheory;
		this.maxScorePractice = maxScorePractice;
		this.examResultList = examResultList;
		this.examFileList = examFileList;
	}

	@Override
	public String toString() {
		return "ExamDTO [examDate=" + examDate + ", examTitle=" + examTitle + ", examState=" + examState
				+ ", maxScoreTheory=" + maxScoreTheory + ", maxScorePractice=" + maxScorePractice + ", examResultList="
				+ examResultList + ", examFileList=" + examFileList + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((examDate == null) ? 0 : examDate.hashCode());
		result = prime * result + ((examFileList == null) ? 0 : examFileList.hashCode());
		result = prime * result + ((examResultList == null) ? 0 : examResultList.hashCode());
		result = prime * result + ((examState == null) ? 0 : examState.hashCode());
		result = prime * result + ((examTitle == null) ? 0 : examTitle.hashCode());
		result = prime * result + ((maxScorePractice == null) ? 0 : maxScorePractice.hashCode());
		result = prime * result + ((maxScoreTheory == null) ? 0 : maxScoreTheory.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamDTO other = (ExamDTO) obj;
		if (examDate == null) {
			if (other.examDate != null)
				return false;
		} else if (!examDate.equals(other.examDate))
			return false;
		if (examFileList == null) {
			if (other.examFileList != null)
				return false;
		} else if (!examFileList.equals(other.examFileList))
			return false;
		if (examResultList == null) {
			if (other.examResultList != null)
				return false;
		} else if (!examResultList.equals(other.examResultList))
			return false;
		if (examState == null) {
			if (other.examState != null)
				return false;
		} else if (!examState.equals(other.examState))
			return false;
		if (examTitle == null) {
			if (other.examTitle != null)
				return false;
		} else if (!examTitle.equals(other.examTitle))
			return false;
		if (maxScorePractice == null) {
			if (other.maxScorePractice != null)
				return false;
		} else if (!maxScorePractice.equals(other.maxScorePractice))
			return false;
		if (maxScoreTheory == null) {
			if (other.maxScoreTheory != null)
				return false;
		} else if (!maxScoreTheory.equals(other.maxScoreTheory))
			return false;
		return true;
	}

}
