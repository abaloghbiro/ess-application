package hu.braininghub.bh06.ess.dto;

import java.io.Serializable;
import java.util.Date;

public class SessionDTO extends BusinessObjectDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date dtStart;
	private Date dtEnd;
	private BhUserDTO userId;

	public SessionDTO() {
		super();
	}

	public Date getDtStart() {
		return dtStart;
	}

	public void setDtStart(Date dtStart) {
		this.dtStart = dtStart;
	}

	public Date getDtEnd() {
		return dtEnd;
	}

	public void setDtEnd(Date dtEnd) {
		this.dtEnd = dtEnd;
	}

	public BhUserDTO getUserId() {
		return userId;
	}

	public void setUserId(BhUserDTO userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dtEnd == null) ? 0 : dtEnd.hashCode());
		result = prime * result + ((dtStart == null) ? 0 : dtStart.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SessionDTO other = (SessionDTO) obj;
		if (dtEnd == null) {
			if (other.dtEnd != null)
				return false;
		} else if (!dtEnd.equals(other.dtEnd))
			return false;
		if (dtStart == null) {
			if (other.dtStart != null)
				return false;
		} else if (!dtStart.equals(other.dtStart))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
