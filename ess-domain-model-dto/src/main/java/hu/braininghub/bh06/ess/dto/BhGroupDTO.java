package hu.braininghub.bh06.ess.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BhGroupDTO extends BusinessObjectDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private List<BhUserDTO> bhUserList = new ArrayList<>();

	public BhGroupDTO() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BhUserDTO> getBhUserList() {
		return bhUserList;
	}

	public void setBhUserList(List<BhUserDTO> bhUserList) {
		this.bhUserList = bhUserList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BhGroupDTO other = (BhGroupDTO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BhGroupDTO [name=" + name + ", bhUserList=" + bhUserList + "]";
	}

}
