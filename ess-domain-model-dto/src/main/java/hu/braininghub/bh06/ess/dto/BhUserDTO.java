package hu.braininghub.bh06.ess.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BhUserDTO extends BusinessObjectDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userName;
	private String firstName;
	private String lastName;
	private String bhPass;
	private Integer active;
	private List<ExamAttendanceDTO> examResultList;
	private List<SessionDTO> sessionList = new ArrayList<>();
	private List<ExamFileDTO> examFileList = new ArrayList<>();
	private BhGroupDTO group;
	private RightDTO right;
	private String emailAddress;

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public BhUserDTO() {
		super();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBhPass() {
		return bhPass;
	}

	public void setBhPass(String bhPass) {
		this.bhPass = bhPass;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public List<ExamAttendanceDTO> getExamResultList() {
		return examResultList;
	}

	public void setExamResultList(List<ExamAttendanceDTO> examResultList) {
		this.examResultList = examResultList;
	}

	public List<SessionDTO> getSessionList() {
		return sessionList;
	}

	public void setSessionList(List<SessionDTO> sessionList) {
		this.sessionList = sessionList;
	}

	public List<ExamFileDTO> getExamFileList() {
		return examFileList;
	}

	public void setExamFileList(List<ExamFileDTO> examFileList) {
		this.examFileList = examFileList;
	}

	public BhGroupDTO getGroup() {
		return group;
	}

	public void setGroup(BhGroupDTO group) {
		this.group = group;
	}

	public RightDTO getRight() {
		return right;
	}

	public void setRight(RightDTO right) {
		this.right = right;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + ((bhPass == null) ? 0 : bhPass.hashCode());
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + ((examFileList == null) ? 0 : examFileList.hashCode());
		result = prime * result + ((examResultList == null) ? 0 : examResultList.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((right == null) ? 0 : right.hashCode());
		result = prime * result + ((sessionList == null) ? 0 : sessionList.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BhUserDTO other = (BhUserDTO) obj;
		if (active == null) {
			if (other.active != null)
				return false;
		} else if (!active.equals(other.active))
			return false;
		if (bhPass == null) {
			if (other.bhPass != null)
				return false;
		} else if (!bhPass.equals(other.bhPass))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (examFileList == null) {
			if (other.examFileList != null)
				return false;
		} else if (!examFileList.equals(other.examFileList))
			return false;
		if (examResultList == null) {
			if (other.examResultList != null)
				return false;
		} else if (!examResultList.equals(other.examResultList))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (right == null) {
			if (other.right != null)
				return false;
		} else if (!right.equals(other.right))
			return false;
		if (sessionList == null) {
			if (other.sessionList != null)
				return false;
		} else if (!sessionList.equals(other.sessionList))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BhUserDTO [userName=" + userName + ", firstName=" + firstName + ", lastName=" + lastName + ", bhPass="
				+ bhPass + ", active=" + active + ", examResultList=" + examResultList + ", sessionList=" + sessionList
				+ ", examFileList=" + examFileList + ", group=" + group + ", right=" + right + ", emailAddress="
				+ emailAddress + "]";
	}

}
