package hu.braininghub.bh06.ess.event;

import hu.braininghub.bh06.ess.dto.EmailDTO;

public interface EmailEventObserver {

	void observeEmail(EmailDTO emailDTO);
}
