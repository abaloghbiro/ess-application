package hu.braininghub.bh06.ess.service;

import java.util.List;

import javax.ejb.Local;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.NameInUseException;

@Local
public interface UserService {

	BhUserDTO getBhUserDTOById(Long id) throws BusinessObjectNotFoundException, DatabaseException;
	
	BhUserDTO getBhUserDTOByUsername(String userName) throws BusinessObjectNotFoundException, DatabaseException;
	
	List<BhUserDTO> getAllBhUserDTO() throws DatabaseException;

	void updateBhUser(BhUserDTO bhUserDTO) throws DatabaseException, BusinessObjectNotFoundException;
	
	void createNewUser(BhUserDTO bhUserDTO) throws NameInUseException, DatabaseException;

	void createNewUser(String userName, String firstName, String lastName, String password, AccessLevels accessLevel,
			String groupName,String email) throws NameInUseException, DatabaseException;

	void changeUserPassword(String username, String newPassword) throws BusinessObjectNotFoundException, DatabaseException;
	
	public List<BhUserDTO> getAllStudents();
	
	
	/***
	 * Returns a list of BhUserDTO-s that share the exam attribute.
	 * @param examID
	 * @return
	 * @throws BusinessObjectNotFoundException
	 * @throws DatabaseException 
	 */
	List<BhUserDTO> getStudentsByExam(Long examID) throws BusinessObjectNotFoundException, DatabaseException;
	
}
