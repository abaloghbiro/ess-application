package hu.braininghub.bh06.ess.service;

import java.util.List;

import javax.ejb.Local;

import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;
import hu.braininghub.bh06.ess.service.exception.NameInUseException;

@Local
public interface GroupService {

	BhGroupDTO getBhGroupDTOById(Long id) throws BusinessObjectNotFoundException;
	
	BhGroupDTO getBhGroupDTOByName(String groupName) throws  DatabaseException;
	
	List<BhGroupDTO> getBhGroupDTOs() throws DatabaseException;
	
	void updateBhGroup(BhGroupDTO bhGroupDTO) throws DatabaseException;
	
	void addNewGroup(BhGroupDTO bhGroupDTO) throws DatabaseException, NameInUseException;
	
}
