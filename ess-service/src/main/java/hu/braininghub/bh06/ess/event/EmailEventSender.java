package hu.braininghub.bh06.ess.event;

import hu.braininghub.bh06.ess.dto.EmailDTO;

public interface EmailEventSender {

	void sendEmail(EmailDTO emailDTO);
}
