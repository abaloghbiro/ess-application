package hu.braininghub.bh06.ess.service;

import java.util.List;

import javax.ejb.Local;

import hu.braininghub.bh06.ess.dto.ExamFileDTO;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

@Local
public interface ExamFileService {
	
	ExamFileDTO getExamFileDTO(Long id) throws  DatabaseException;
	
	List<ExamFileDTO> getAllExamFileDTO() throws DatabaseException;

	void updateExamFile(ExamFileDTO examFileDTO) throws  DatabaseException;

	void addNewExamFile(ExamFileDTO examFileDTO) throws DatabaseException;
}
