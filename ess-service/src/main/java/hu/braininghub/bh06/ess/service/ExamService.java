package hu.braininghub.bh06.ess.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;
import hu.braininghub.bh06.ess.dto.ExamDTO;
import hu.braininghub.bh06.ess.service.exception.BusinessObjectNotFoundException;
import hu.braininghub.bh06.ess.service.exception.DatabaseException;

@Local
public interface ExamService {
	
	ExamDTO getExamDTO(Long id) throws  DatabaseException;
	
	ExamAttendanceDTO getExamAttendanceDTO(Long id) throws  DatabaseException;
	
	List<ExamAttendanceDTO> getAllExamAttendanceDTO() throws DatabaseException;
	
	void updateExamAttendance(ExamAttendanceDTO examAttendanceDTO) throws Exception;

	List<ExamDTO> getAllExamDTO() throws DatabaseException;

	void updateExam(ExamDTO examDTO) throws DatabaseException;

	void addNewExam(ExamDTO examDTO) throws DatabaseException;

	/****
	 * The String examDate must have the date in the "yyyy-MM-dd-hh-mm-ss" format.
	 * @throws ParseException 
	 * @throws DatabaseException 
	 */
	void createNewExam(String examDate, String examTitle, BigDecimal maxScoreTheory, BigDecimal maxScorePractice) throws ParseException, DatabaseException;

	Map<Integer, List<String>> getExamDetailsByExam(Long examId) throws BusinessObjectNotFoundException ;

	Map<Integer, List<String>> getExamDetailsByUser(BhUserDTO user);

	List<ExamDTO> getExamsByGroup(Long groupId) throws DatabaseException ;

	void linkExamToGroup(Long examId, Long groupId) throws  DatabaseException, BusinessObjectNotFoundException;
	
	public Map<String, Map<String, Double>> getAllAverageExamResults() throws  DatabaseException;
	
	/**
	 * Returns a HashMap containing the usernames as keys and
	 * a list containing the average result per exam.
	 * @throws BusinessObjectNotFoundException 
	 * @throws DatabaseException 
	 */
	public Map<String, Double> getAverageExamResultsByGroup(String groupName) throws  DatabaseException;
}
