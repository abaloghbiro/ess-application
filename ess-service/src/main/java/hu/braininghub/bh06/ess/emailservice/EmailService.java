package hu.braininghub.bh06.ess.emailservice;

public interface EmailService {

	void sendEmail(String to, String subject, String body);
}
