package hu.braininghub.bh06.ess.service.exception;

public class BusinessObjectNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BusinessObjectNotFoundException(String message) {
		super(message);
	}
	
	

}
