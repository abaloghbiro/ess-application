package hu.braininghub.bh06.ess.service.exception;

public class NameInUseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NameInUseException(String message) {
		super(message);
	}



}
