package hu.braininghub.bh06.ess.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;
import hu.braininghub.bh06.ess.dto.BhGroupDTO;
import hu.braininghub.bh06.ess.dto.BhUserDTO;
import hu.braininghub.bh06.ess.dto.RightDTO;


@Ignore
public class TestBaseDAO {

	private EntityManager em;
	private static EntityManagerFactory factory;
	private static final String PU_NAME = "essTestPU";

	@BeforeClass
	public static void initFactory() {
		factory = Persistence.createEntityManagerFactory(PU_NAME);
	}

	@Before
	public void init() {
		em = factory.createEntityManager();
	}


	@Test
	public void testInserUser() {

		try {
			BhUserDAO userDAO = new BhUserDefaultDAO();
			((BhUserDefaultDAO) userDAO).setupEntityManager(em);

			BhGroupDAO groupDAO = new BhGroupDefaultDAO();
			((BhGroupDefaultDAO) groupDAO).setupEntityManager(em);

			RightDAO rightDAO = new RightDefaultDAO();
			((RightDefaultDAO) rightDAO).setupEntityManager(em);

			em.getTransaction().begin();

			BhUserDTO user = new BhUserDTO();
			user.setActive(1);
			user.setBhPass("admin123");
			user.setEmailAddress("test@test.hu");
			user.setFirstName("Attila");
			user.setLastName("Balogh-Bir�");
			user.setUserName("abaloghbiro");

			BhGroupDTO bh05Group = groupDAO.getByName("BH05");
			assertNotNull(bh05Group);

			user.setGroup(bh05Group);

			RightDTO right = rightDAO.getByAccessLevels(AccessLevels.STUDENT);
			user.setRight(right);

			userDAO.insert(user);

			BhUserDTO loadedUser = userDAO.getById(1L);

			bh05Group.getBhUserList().add(loadedUser);

			groupDAO.update(bh05Group);

			em.getTransaction().commit();

			bh05Group = groupDAO.getByName("BH05");

			assertEquals(2, bh05Group.getBhUserList().size());

		} catch (Exception ex) {

			if (em != null && em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}

			ex.printStackTrace();
		}

	}
}
