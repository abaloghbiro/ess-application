INSERT INTO RIGHTS (ID, ACCESS_LEVEL, BHUSERLIST) VALUES (1, 'STUDENT', null)
INSERT INTO RIGHTS (ID, ACCESS_LEVEL, BHUSERLIST) VALUES (2, 'INSTRUCTOR', null)
INSERT INTO RIGHTS (ID, ACCESS_LEVEL, BHUSERLIST) VALUES (3, 'ADMIN', null)
INSERT INTO GROUPS (ID, NAME) VALUES (4, 'instructor')
INSERT INTO GROUPS (ID, NAME) VALUES (5, 'admin')
INSERT INTO GROUPS (ID, NAME) VALUES (6, 'student')
INSERT INTO EXAMS (ID, EXAM_DATE, EXAM_STATE, EXAM_TITLE, MAX_SCORE_PRACTICE, MAX_SCORE_THEORY) VALUES (7, TO_TIMESTAMP('2018-06-25 18:06:42.639', 'YYYY-MM-DD HH24:MI:SS.FF'), 0, 'ALAPOZAS', null, null)
INSERT INTO EXAMS (ID, EXAM_DATE, EXAM_STATE, EXAM_TITLE, MAX_SCORE_PRACTICE, MAX_SCORE_THEORY) VALUES (8, TO_TIMESTAMP('2018-06-25 18:06:42.639', 'YYYY-MM-DD HH24:MI:SS.FF'), 0, 'ZARO�', null, null)
INSERT INTO EXAMS (ID, EXAM_DATE, EXAM_STATE, EXAM_TITLE, MAX_SCORE_PRACTICE, MAX_SCORE_THEORY) VALUES (9, TO_TIMESTAMP('2018-06-25 18:06:42.639', 'YYYY-MM-DD HH24:MI:SS.FF'), 0, 'OOP', null, null)
INSERT INTO BH_USER (ID, ACTIVE, BH_PASS, EMAIL, FIRST_NAME, LAST_NAME,USER_NAME, GROUP_ID, RIGHT_ID) VALUES (10,1,'JAvlGPq9JyTdtvBO6x2llnRI1+gxwIyPqCKAn3THIKk=','abc1@gmail.com', 'KISS', 'PETER','pkiss',4,3)
INSERT INTO BH_USER (ID, ACTIVE, BH_PASS, EMAIL, FIRST_NAME, LAST_NAME,USER_NAME, GROUP_ID, RIGHT_ID) VALUES (11,1,'JAvlGPq9JyTdtvBO6x2llnRI1+gxwIyPqCKAn3THIKk=','abc2@gmail.com', 'NAGY', 'ANDRAS','anagy',5,1)
INSERT INTO BH_USER (ID, ACTIVE, BH_PASS, EMAIL, FIRST_NAME, LAST_NAME,USER_NAME, GROUP_ID, RIGHT_ID) VALUES (12,1,'JAvlGPq9JyTdtvBO6x2llnRI1+gxwIyPqCKAn3THIKk=','abaloghbiro@protonmail.com', 'BALOGH-BIRO', 'ATTILA','abaloghbiro',4,2)
INSERT INTO BH_USER (ID, ACTIVE, BH_PASS, EMAIL, FIRST_NAME, LAST_NAME,USER_NAME, GROUP_ID, RIGHT_ID) VALUES (13,1,'JAvlGPq9JyTdtvBO6x2llnRI1+gxwIyPqCKAn3THIKk=','abc3@gmail.com', 'LAKATOS', 'PETER','alakatos',6,1)
INSERT INTO BH_USER (ID, ACTIVE, BH_PASS, EMAIL, FIRST_NAME, LAST_NAME,USER_NAME, GROUP_ID, RIGHT_ID) VALUES (14,1,'JAvlGPq9JyTdtvBO6x2llnRI1+gxwIyPqCKAn3THIKk=','abc3@gmail.com', 'P�RTOS', 'BAL�ZS','apartos',6,1)
INSERT INTO EXAM_ATTENDANCE (ID, SCORE_PRACTICE, SCORE_THEORY, EXAM_ID, USER_ID) VALUES(18,null,null,7,11)
INSERT INTO EXAM_ATTENDANCE (ID, SCORE_PRACTICE, SCORE_THEORY, EXAM_ID, USER_ID) VALUES(20,null,null,8,13)
INSERT INTO EXAM_ATTENDANCE (ID, SCORE_PRACTICE, SCORE_THEORY, EXAM_ID, USER_ID) VALUES(21,null,null,8,14)

