package hu.braininghub.bh06.ess.dao;


import javax.ejb.Stateless;

import hu.braininghub.bh06.ess.dao.model.Exam;
import hu.braininghub.bh06.ess.dto.ExamDTO;

@Stateless
public class DefaultExamFileDAO extends JpaDefaultBaseDAO<Exam,ExamDTO> implements ExamDAO{


	public ExamDTO getExamByTitle(String title) {
		  
		return (ExamDTO) entityMapper.transformEntity((Exam) em.createQuery("SELECT r FROM Exam r WHERE r.examTitle = :name")
				.setParameter("name", title).getSingleResult());
	}

}