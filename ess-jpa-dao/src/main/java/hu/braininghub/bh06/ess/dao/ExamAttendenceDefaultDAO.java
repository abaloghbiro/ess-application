package hu.braininghub.bh06.ess.dao;

import javax.ejb.Stateless;

import hu.braininghub.bh06.ess.dao.model.ExamAttendance;
import hu.braininghub.bh06.ess.dto.ExamAttendanceDTO;

@Stateless
public class ExamAttendenceDefaultDAO extends JpaDefaultBaseDAO<ExamAttendance,ExamAttendanceDTO> implements ExamAttendanceDAO{


}