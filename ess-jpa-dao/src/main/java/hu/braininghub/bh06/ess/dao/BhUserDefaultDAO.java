package hu.braininghub.bh06.ess.dao;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;

import hu.braininghub.bh06.ess.dao.model.BhUser;
import hu.braininghub.bh06.ess.dto.BhUserDTO;

@Stateless
public class BhUserDefaultDAO extends JpaDefaultBaseDAO<BhUser, BhUserDTO> implements BhUserDAO {

	@Override
	public Optional<BhUserDTO> getByUserName(String userName) {

		List<BhUser> users = em.createQuery("SELECT r FROM BhUser r WHERE r.userName = :name", BhUser.class)
				.setParameter("name", userName).getResultList();

		BhUserDTO dto = null;

		if (users != null && !users.isEmpty()) {
			dto = entityMapper.transformEntity(users.get(0));
		}

		return Optional.ofNullable(dto);
	}

}