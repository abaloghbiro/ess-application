package hu.braininghub.bh06.ess.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.core.GenericTypeResolver;

import hu.braininghub.bh06.ess.dao.mapper.EntityMapper;
import hu.braininghub.bh06.ess.dao.model.BusinessObject;
import hu.braininghub.bh06.ess.dto.BusinessObjectDTO;

public abstract class JpaDefaultBaseDAO<E extends BusinessObject, DTO extends BusinessObjectDTO>
		implements BaseDAO<DTO> {

	@PersistenceContext(name = "essPU")
	protected EntityManager em;
	protected Class<E> entityClazz;
	protected EntityMapper<E, DTO> entityMapper;

	public void setupEntityManager(EntityManager em) {
		this.em = em;
	}

	@SuppressWarnings("unchecked")
	public JpaDefaultBaseDAO() {
		
		Class<?>[] clazzes = GenericTypeResolver.resolveTypeArguments(getClass(), JpaDefaultBaseDAO.class);

		Class<DTO> dtoClazz = null;

		if (clazzes != null && clazzes.length == 2) {

			for (Class<?> c : clazzes) {

				if (BusinessObject.class.isAssignableFrom(c)) {
					entityClazz = (Class<E>) c;
				} 
				else if (BusinessObjectDTO.class.isAssignableFrom(c)) {
					dtoClazz = (Class<DTO>) c;
				}
			}

		} else {
			throw new IllegalArgumentException("Couldn't determine business object or dto types!");
		}

		entityMapper = new EntityMapper<E, DTO>(entityClazz, dtoClazz);
	}

	public Long merge(DTO object) {
		E entity = entityMapper.transformDTO(object);
		if (!em.contains(entity)) {
			em.merge(entity);
			em.flush();
		}
		return entity.getId();
	}

	public void delete(DTO object) {
		E entity = entityMapper.transformDTO(object);
		em.remove(em.merge(entity));
	}

	@SuppressWarnings("unchecked")
	public List<DTO> getAll() {
		return entityMapper
				.transformEntities(em.createQuery("Select e from " + entityClazz.getSimpleName() + " e").getResultList());
	}

	public DTO getById(Long id) {
		E entity = em.find(entityClazz, id);
		return entityMapper.transformEntity(entity);
	}

	public void update(DTO object) {
		merge(object);

	}

	public Long insert(DTO object) {
		return merge(object);
	}
}
