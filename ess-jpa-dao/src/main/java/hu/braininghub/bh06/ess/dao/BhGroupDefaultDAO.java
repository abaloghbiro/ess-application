package hu.braininghub.bh06.ess.dao;


import javax.ejb.Stateless;

import hu.braininghub.bh06.ess.dao.model.Groups;
import hu.braininghub.bh06.ess.dto.BhGroupDTO;


@Stateless
public class BhGroupDefaultDAO extends JpaDefaultBaseDAO<Groups,BhGroupDTO> implements BhGroupDAO{
	

	public BhGroupDTO getByName(String name) {
		return (BhGroupDTO) entityMapper.transformEntity((Groups) em.createQuery("SELECT g FROM Groups g WHERE g.name = :name")
						.setParameter("name", name).getSingleResult());
	}

	
	
	
	
}