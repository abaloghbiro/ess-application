package hu.braininghub.bh06.ess.dao;

import javax.ejb.Stateless;

import hu.braininghub.bh06.ess.common.auth.AccessLevels;
import hu.braininghub.bh06.ess.dao.model.Right;
import hu.braininghub.bh06.ess.dto.RightDTO;


@Stateless
public class RightDefaultDAO extends JpaDefaultBaseDAO<Right,RightDTO> implements RightDAO{

	public RightDefaultDAO() {
		super();
	}
	
	public RightDTO getByAccessLevels(AccessLevels level) {
		return (RightDTO) entityMapper.transformEntity((Right) em.createQuery("SELECT r FROM Right r WHERE r.accessLevel = " + AccessLevels.class.getName()+ "."+level).getSingleResult());
	}

}