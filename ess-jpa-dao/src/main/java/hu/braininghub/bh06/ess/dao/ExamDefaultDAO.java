package hu.braininghub.bh06.ess.dao;

import javax.ejb.Stateless;

import hu.braininghub.bh06.ess.dao.model.ExamFile;
import hu.braininghub.bh06.ess.dto.ExamFileDTO;

@Stateless
public class ExamDefaultDAO extends JpaDefaultBaseDAO<ExamFile, ExamFileDTO> implements ExamFileDAO {

}