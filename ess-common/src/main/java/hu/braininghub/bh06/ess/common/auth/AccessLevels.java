package hu.braininghub.bh06.ess.common.auth;

public enum AccessLevels {

	STUDENT(1), INSTRUCTOR(2), ADMIN(3);

	private int level;

	private AccessLevels(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}
}
